package com.reportespms.process;

import com.connectionsfffm.objetos.OutputsFFM;

import com.reportespms.baseobjects.Constantes;
import com.reportespms.baseobjects.Variables;
import com.reportespms.inputs.InputStatus;
import com.reportespms.logica.LogicaGraficaReporteGeneral;
import com.reportespms.objects.Catalogo;
import com.reportespms.objects.ReporteGeneral;

import com.reportespms.outputs.OutputStatus;

import com.reportespms.utils.LocalUtils;

import java.util.Map;

import javax.ws.rs.core.Response;

public class CatalogoStatus {
    public CatalogoStatus() {
        super();
        LocalUtils.loadConfigIN_DB();
    }
    
    public Response getStatusOTs(String inputStatus){
        
        InputStatus input_ws = (InputStatus) Constantes.JAR_UTILS.strJsonToClass(inputStatus, InputStatus.class);
        OutputStatus output_ws = new OutputStatus();
        
        try{
            String consulta = "";
            if(input_ws.getIdNegocio().equals("1")){
                consulta = Variables.getQR_STATUS_REC();
            }else{
                consulta = Variables.getQR_STATUS_ENL();
            }
            
            OutputsFFM outputFfm =  Constantes.CONECTION_FFM.consultaBD(consulta);
            
            if(outputFfm.getResult()){
                String valoresCatalogo[][] = outputFfm.getArr_records();
                if(valoresCatalogo.length>0){
                    Catalogo catArr[] = new Catalogo[valoresCatalogo.length];
                    for(int fila = 0; fila <valoresCatalogo.length; fila++){
                        Catalogo cat = new Catalogo();
                        cat.setId(valoresCatalogo[fila][0]);
                        cat.setDescripcion(valoresCatalogo[fila][1]);
                        cat.setNivel(valoresCatalogo[fila][2]);
                        cat.setIdPadre(valoresCatalogo[fila][3]);
                        catArr[fila] = cat;
                    }  
                    output_ws.setResult(Constantes.RESULT_SUSSES);
                    output_ws.setResultDescripcion(Constantes.RESULTSESCRIPTION_SUSSES);
                    output_ws.setCatalogos(catArr);
                }else{
                    output_ws.setResult(Constantes.RESULT_SUSSES);
                    output_ws.setResultDescripcion("No se encontraron Resultados");
                }
            }else{
                output_ws.setResult(Constantes.RESULT_ERROR);
                output_ws.setResultDescripcion("Error con ffm : " + outputFfm.getResultDescripcion());
            }
        }catch(Exception ex){
            output_ws.setResult(Constantes.RESULT_ERROR);
            output_ws.setResultDescripcion("Error ex :" + ex.toString());
        }catch(Throwable tx){
            output_ws.setResult(Constantes.RESULT_ERROR);
            output_ws.setResultDescripcion("Error tx : " + tx.toString());
        }
        
        return Response.status(200).entity(Constantes.JAR_UTILS.classTostrJson(output_ws)).build();
    }
}
