package com.reportespms.process;

import com.reportespms.baseobjects.Constantes;
import com.reportespms.inputs.InputReporteGeneral;
import com.reportespms.logica.LogicaReporteGeneral;
import com.reportespms.objects.OT;
import com.reportespms.objects.Planeacion;
import com.reportespms.objects.ReporteGeneral;
import com.reportespms.outputs.OutputReporteGeneral;
import com.reportespms.utils.LocalUtils;

import java.util.List;
import java.util.Map;

import javax.ws.rs.core.Response;

public class ReporteGeneralPMS {
    public ReporteGeneralPMS() {
        super();
        LocalUtils.loadConfigIN_DB();
    }
    
    public Response ReporteGeneralPMs(String InputReporteGeneral){
        
        InputReporteGeneral input_ws = (InputReporteGeneral) Constantes.JAR_UTILS.strJsonToClass(InputReporteGeneral, InputReporteGeneral.class);
        OutputReporteGeneral output_ws = new OutputReporteGeneral();
        
        try{
            Map<String,ReporteGeneral> mapPaquetesSf = LogicaReporteGeneral.loadDataSF(input_ws);
            List<List<String>> listOSsRec = LogicaReporteGeneral.getListOSsRec(mapPaquetesSf);
            List<List<String>> listOSsEnl = LogicaReporteGeneral.getListOSsEnl(mapPaquetesSf);
            List<List<String>> listaCsps = LogicaReporteGeneral.getListCsp(mapPaquetesSf);
            Map<String,OT> mapOTsRec = LogicaReporteGeneral.loadOTsRec(listOSsRec);
            Map<String,OT> mapOTsEnl = LogicaReporteGeneral.loadOTsEnl(listOSsEnl);
            Map<String,Planeacion> mapPlaneacionCsp = LogicaReporteGeneral.loadPlaneacionCsp(listaCsps);
            
                        
            output_ws.setResult(Constantes.RESULT_SUSSES);
            output_ws.setResultDescripcion(Constantes.RESULTSESCRIPTION_SUSSES);
            output_ws.setReporteGeneral(LogicaReporteGeneral.loadFullReport(mapPaquetesSf, mapOTsRec, mapOTsEnl,mapPlaneacionCsp).values().toArray(new ReporteGeneral[mapPaquetesSf.size()]));
            
        }catch(Exception ex){
            output_ws.setResult(Constantes.RESULT_ERROR);
            output_ws.setResultDescripcion("Error : " + ex.toString());
            output_ws.setVersion(Constantes.WS_VERSION);
        }catch(Throwable tx){
            output_ws.setResult(Constantes.RESULT_ERROR);
            output_ws.setResultDescripcion("Error : " + tx.toString());
            output_ws.setVersion(Constantes.WS_VERSION);
        }
        
        System.out.println(Constantes.JAR_UTILS.classTostrJson(output_ws));
        
        return Response.status(200).entity(Constantes.JAR_UTILS.classTostrJson(output_ws)).build();
    }
}
