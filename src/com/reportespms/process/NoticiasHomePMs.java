package com.reportespms.process;

import com.reportespms.baseobjects.Constantes;

import com.reportespms.hilos.HiloActividadesMes;
import com.reportespms.hilos.HiloCambiosStatusOS;
import com.reportespms.hilos.HiloNuevosProyectos;
import com.reportespms.hilos.HiloProyectos;
import com.reportespms.inputs.InputNoticiasHomePMs;
import com.reportespms.objects.Hoy;
import com.reportespms.objects.Implementacion;
import com.reportespms.outputs.OutputNoticiasHomePMs;
import com.reportespms.utils.LocalUtils;

import java.util.ArrayList;

import java.util.Map;

import javax.ws.rs.core.Response;

public class NoticiasHomePMs {
    public NoticiasHomePMs() {
        super();
        LocalUtils.loadConfigIN_DB();
    }
    
    public Response loadNewsPMs(String InputNoticiasHomePMs){
        
        InputNoticiasHomePMs input_ws = (InputNoticiasHomePMs) Constantes.JAR_UTILS.strJsonToClass(InputNoticiasHomePMs, InputNoticiasHomePMs.class);
        OutputNoticiasHomePMs output_ws = new OutputNoticiasHomePMs();
        
        try{
            
            Map<String,Implementacion> mapProyetos = new HiloProyectos().loadDataProyecto(input_ws.getIdPm());
            
            HiloNuevosProyectos hiloProyectosNuevos = new HiloNuevosProyectos(input_ws.getIdPm());
            HiloCambiosStatusOS hiloCambiosImplementacion = new HiloCambiosStatusOS(input_ws.getIdPm());
            HiloActividadesMes hiloActividades = new HiloActividadesMes(input_ws.getIdPm(),mapProyetos);
            ArrayList<Hoy> listNotificacionesHoy = new ArrayList<Hoy>();
            ArrayList<Hoy> listNotificacionesProyectos = new ArrayList<Hoy>();
            ArrayList<Hoy> listNotificacionesImplementacion = new ArrayList<Hoy>();
            ArrayList<Hoy> listNotificacionesActividades = new ArrayList<Hoy>();
            
            hiloProyectosNuevos.start();
            hiloCambiosImplementacion.start();
            hiloActividades.start();
            
            hiloProyectosNuevos.join();
            hiloCambiosImplementacion.join();
            hiloActividades.join();
            
            if(hiloProyectosNuevos.getListNotificacionesHoy()!=null){
                listNotificacionesHoy.addAll(hiloProyectosNuevos.getListNotificacionesHoy());
            }
            if(hiloCambiosImplementacion.getListNotificacionesHoy()!=null){
                listNotificacionesHoy.addAll(hiloCambiosImplementacion.getListNotificacionesHoy());
            }
            if(hiloActividades.getListNotificacionesHoy()!=null){
                listNotificacionesHoy.addAll(hiloActividades.getListNotificacionesHoy());        
            }
            
            
            if(hiloProyectosNuevos.getListNotificacionesProyectos()!=null){
                listNotificacionesProyectos.addAll(hiloProyectosNuevos.getListNotificacionesProyectos());
            }
            
            if(hiloCambiosImplementacion.getListNotificacionesImplementacion()!=null){
                listNotificacionesImplementacion.addAll(hiloCambiosImplementacion.getListNotificacionesImplementacion());
            }
            
            if(hiloActividades.getListNotificacionesActividades()!=null){
               listNotificacionesActividades.addAll(hiloActividades.getListNotificacionesActividades());
            }
            
            output_ws.setResult(Constantes.RESULT_SUSSES);
            output_ws.setResultDescripcion(Constantes.RESULTSESCRIPTION_SUSSES);
            output_ws.setVersion(Constantes.WS_VERSION);
            output_ws.setNotificacionesHoy(listNotificacionesHoy.toArray(new Hoy[listNotificacionesHoy.size()]));
            output_ws.setNotificacioneProyectos(listNotificacionesProyectos.toArray(new Hoy[listNotificacionesProyectos.size()]));
            output_ws.setNotificacionesImplementacion(listNotificacionesImplementacion.toArray(new Hoy[listNotificacionesImplementacion.size()]));
            output_ws.setNotificacionesActividades(listNotificacionesActividades.toArray(new Hoy[listNotificacionesActividades.size()]));
            output_ws.setDetalleProyectosMes(hiloProyectosNuevos.getArrNuevosProyectos());
            output_ws.setDetalleImplementacionMes(hiloCambiosImplementacion.getDetalleCambiosImplementacion());
            output_ws.setDetalleActividadesMes(hiloActividades.getDetalleActividadesPM());
            
        }catch(Exception ex){
            output_ws.setResult(Constantes.RESULT_ERROR);
            output_ws.setResultDescripcion("Error ex : " + ex.toString());
            output_ws.setVersion(Constantes.WS_VERSION);
        }catch(Throwable ex){
            output_ws.setResult(Constantes.RESULT_ERROR);
            output_ws.setResultDescripcion("Error thr : " + ex.toString());
            output_ws.setVersion(Constantes.WS_VERSION);
        }
        
        System.out.println(Constantes.JAR_UTILS.classTostrJson(output_ws));
        
        return Response.status(200).entity(output_ws).build();            
    }
}
