package com.reportespms.process;

import com.reportespms.baseobjects.Constantes;

import com.reportespms.inputs.InputGraficaReporteGeneralPMs;

import com.reportespms.logica.LogicaGraficaReporteGeneral;
import com.reportespms.objects.ReporteGeneral;
import com.reportespms.outputs.OutputGraficaReporteGeneralPMs;

import com.reportespms.utils.LocalUtils;

import java.util.Map;

import javax.ws.rs.core.Response;

public class GraficaReporteGeneralPMs {
    public GraficaReporteGeneralPMs() {
        super();
        LocalUtils.loadConfigIN_DB();
    }
    
    public Response graficaReporteGeneralPMs(String InputGraficaReporteGeneralPMs){
        
        InputGraficaReporteGeneralPMs input_ws = (InputGraficaReporteGeneralPMs) Constantes.JAR_UTILS.strJsonToClass(InputGraficaReporteGeneralPMs, InputGraficaReporteGeneralPMs.class);
        OutputGraficaReporteGeneralPMs output_ws = new OutputGraficaReporteGeneralPMs();
        
        try{
            
            Map<String,ReporteGeneral> mapInfoSf = LogicaGraficaReporteGeneral.loadDataSF(input_ws,Constantes.C_SF_GRAFICA_GENERAL);
            Map<String,ReporteGeneral> mapInfoMesAnteriroSf = LogicaGraficaReporteGeneral.loadDataSF(input_ws,Constantes.C_SF_GRAFICA_MES_ANTERIOR);
            
            output_ws.setResult(Constantes.RESULT_SUSSES);
            output_ws.setResultDescripcion(Constantes.RESULTSESCRIPTION_SUSSES);
            output_ws.setVersion(Constantes.WS_VERSION);
            output_ws.setDetalleTabla(mapInfoSf.values().toArray(new ReporteGeneral[mapInfoSf.size()]));
            output_ws.setDetalleGraficas(LogicaGraficaReporteGeneral.calculaGraficaGeneral(mapInfoSf, mapInfoMesAnteriroSf));
            
        }catch(Exception ex){
            output_ws.setResult(Constantes.RESULT_ERROR);
            output_ws.setResultDescripcion("Error : " + ex.toString());
            output_ws.setVersion(Constantes.WS_VERSION);
        }catch(Throwable tx){
            output_ws.setResult(Constantes.RESULT_ERROR);
            output_ws.setResultDescripcion("Error : " + tx.toString());
            output_ws.setVersion(Constantes.WS_VERSION);
        }
        
        System.out.println(Constantes.JAR_UTILS.classTostrJson(output_ws));
        
        return Response.status(200).entity(Constantes.JAR_UTILS.classTostrJson(output_ws)).build();
    }
    
    
}
