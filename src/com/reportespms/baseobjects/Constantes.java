package com.reportespms.baseobjects;

import com.connectionsfffm.utils.Utilerias;

import com.reportespms.utils.ConeccionFFM;
import com.reportespms.utils.ConeccionSF;
import com.reportespms.utils.ConectorDB;

import com.reportespms.utils.LocalUtils;

import java.util.Properties;

public class Constantes {
    
    public static final ConectorDB CONECTION_DATA_BASE = new ConectorDB();
    public static final ConeccionFFM CONECTION_FFM = new ConeccionFFM();
    public static final String PATH_PROPERTIES = "/resources/Configuracion.properties";
    public static final Utilerias JAR_UTILS = new Utilerias();
    
    public static final Properties CONFIG_PROPERTIES = new LocalUtils().getConfiguracion();
    public static final String RESULT_SUSSES = CONFIG_PROPERTIES.getProperty("ws.susses_id");
    public static final String CS_CONFIG_IN_DB = CONFIG_PROPERTIES.getProperty("qr.conf_in_db");
    public static final String RESULT_ERROR = CONFIG_PROPERTIES.getProperty("ws.error_id");
    public static final String RESULTSESCRIPTION_SUSSES = CONFIG_PROPERTIES.getProperty("ws.msj_susses");
    public static final String WS_VERSION = CONFIG_PROPERTIES.getProperty("ws.version");
    public static final String CS_ID_SESSION_SF_DB = CONFIG_PROPERTIES.getProperty("c_get_id_session_sf");
    public static final String QR_UPDATE_SI_SF = CONFIG_PROPERTIES.getProperty("c_update_sessionid_sf");
       
    public static final String TIPO_CONEXION = CONFIG_PROPERTIES.getProperty("servidor.type_conection");;
    public static final String DB_HOST = CONFIG_PROPERTIES.getProperty("servidor.host");
    public static final String DB_PORT = CONFIG_PROPERTIES.getProperty("servidor.port");
    public static final String DB_DBNAME = CONFIG_PROPERTIES.getProperty("servidor.namedb");
    public static final String DB_US = CONFIG_PROPERTIES.getProperty("servidor.us");
    public static final String DB_PWR = CONFIG_PROPERTIES.getProperty("servidor.pa");
    public static final String DB_JNDI = CONFIG_PROPERTIES.getProperty("servidor.jndi");
    
    public static final String C_TYPE_INFO  = "000";
    public static final String C_TYPE_SUSSES = "001";
    public static final String C_TYPE_ERR_FFM = "002";
    public static final String C_TYPE_ERR_SF = "003";
    public static final String C_TYPE_GEN_ERR = "004";
    
    //-- Mensaje
    public static final String C_MESSAGE_INFO       = "000 - MSINFO : ";
    public static final String C_MESSAGE_SUSSES     = "001 - SUSSES : ";
    public static final String C_MESSAGE_ERR_FFM    = "002 - FFMERR : ";
    public static final String C_MESSAGE_ERR_SF     = "003 - SF-ERR : ";
    public static final String C_MESSAGE_GENERAL_ERR= "004 - GR-ERR : ";
    
    public static final String SF_QR_VALIDATE = CONFIG_PROPERTIES.getProperty("ws.validate_qr_sf");
    public static final String KEY_US_SF = "AUTENTICATION_SF_US";
    public static final String KEY_PWS_SF = "AUTENTICATION_SF_PS";
    public static final String KEY_ENDPOINT_SF = "END_POINT_WS_SF";
    public static final String KEY_DIAS_PLAN = "DIAS_PLAN";
    public static final String KEY_DIAS_SOLUCION = "DIAS_SOLUCION";
    public static final String KEY_COLOR_OK = "COLOR_PM_OK";
    public static final String KEY_COLOR_M = "COLOR_PM_M";
    public static final String KEY_COLOR_B = "COLOR_PM_B";
    public static final String KEY_COLOR_SB = "COLOR_PM_SB";
    
    public static final String CONECTION_WITH_JNDI = "JNDI";
    public static final String CONECTION_WITH_JDBC = "JDBC";
    
    // Reporte general
    public final static String LABEL_DIA = "D";
    public final static String LABEL_SEMANA = "S";
    public final static String LABEL_MES = "M";
    public static final String C_SF_REPORTE_GENERAL = CONFIG_PROPERTIES.getProperty("qr_sf_reporte_general");
    public static final String C_OTS_ENL_REPORTE_GENERAL = CONFIG_PROPERTIES.getProperty("qr_ffm_enl_ots_reporte_general");
    public static final String C_OTS_REC_REPORTE_GENERAL = CONFIG_PROPERTIES.getProperty("qr_ffm_rec_ots_reporte_general");
    public static final String C_DATOS_PLANEACION_GENRAL = CONFIG_PROPERTIES.getProperty("qr_ffm_datos_planeacion_general");
    public static final String C_LABEL_SOLUCION = "Soluci�n a la medida";
    public static final String C_LABEL_PAQUETE = "Paquete";
    
    // Grafica reporte general
    public static final String C_SF_GRAFICA_GENERAL = CONFIG_PROPERTIES.getProperty("qr_sf_grafica_general");
    public static final String C_SF_GRAFICA_MES_ANTERIOR = CONFIG_PROPERTIES.getProperty("qr_sf_gafica_mes_anterior");
    public static final String C_OS_PENDIENTE = "Pendiente";
    public static final String C_OS_INICIO = "Inicio";
    public static final String C_OS_AGENDADO = "Agendado";
    public static final String C_OS_CONFIRMADO = "Confirmado";
    public static final String C_OS_PROCESO = "En proceso";
    public static final String C_OS_COMPLETADO = "Completado";
    public static final String C_OS_RESCATE = "Rescate";
    public static final String C_OS_GESTORIA = "Gestoria";
    public static final String C_OS_DETENIDA = "Detenida";
    public static final String C_OS_CANCELADO = "Cancelado";
    public static final String C_OS_CALENDARIZADO = "Calendarizado";
    public static final String C_OS_SUSPENDIDA = "Suspendido";
    
    public static final String C_POR_INSTALAR = "Por instalar";
    public static final String C_CALENDARIZADO = "Calendarizado";
    public static final String C_DETENIDO = "Detenido";
    public static final String C_DEVUELTO_VENTAS = "Devuelto a ventas";
    public static final String C_CANCELADO = "Cancelado";
    public static final String C_INSTALADO = "Instalado";
    
    // Notificaciones Home
    public static final String KEY_STATUS_ENL = "STATUS_ENL";
    public static final String KEY_STATUS_REC = "STATUS_REC";
    public static final String KEY_DESC_IMO = "DESCIMP";
    public static final String KEY_BGCOLOR_IMP = "BGCOLORIMP";
    public static final String KEY_TXTCOLOR_IMP = "TXTCOLORIMP";
    public static final String KEY_DESC_ACT = "DESCACTIVIDAD";
    public static final String KEY_BGCOLOR_ACT = "BGCOLORACTIVIDAD";
    public static final String KEY_TXTCOLOR_ACT = "TXTCOLORACTIVIDAD";
    public static final String KEY_DESC_PRO = "DESCPROYECTO";
    public static final String KEY_BGCOLOR_PRO = "BGCOLORPROYECTO";
    public static final String KEY_TXTCOLOR_PRO = "TXTCOLORPROYECTO";
    
    public static final String L_INICIO = "Inicio";
    public static final String L_AGENDADO = "Agendado";
    public static final String L_CONFIRMADO = "Confirmado";
    public static final String L_COMPLETADO = "Completado";
    public static final String L_RESCATE = "Rescate";
    public static final String L_CANCELADO = "Cancelado";
    public static final String L_ENPROCESO = "En proceso";
    public static final String L_CALENDARIZADO = "Calendarizado";
    public static final String L_DETENIDA = "Detenida";
    public static final String L_SUSPENDIDO = "Suspendido";
    public static final String L_GESTORIA = "Gestoria";
    
    public static final String C_DESCRIPCION_PROYECTO_NUEVO = "Nuevo proyecto asignado";
    public static final String C_DESCRIPCION_INICIO_ACTIVIDAD = "Cierre de actividad";
    public static final String C_DESCRIPCION_FIN_ACTIVIDAD = "Inicio de actividad";
    public static final String C_DESCRIPCION_INTOFIN_ACTIVIDAD = "Inicio-Fin actividad";
    public static final String C_DESCRIPCION_CAMBIO_ACTIVIDAD = "Nuevo estatus implementacion";
    public static final String C_DESC_PROYECTO_NUEVO = "Proyecto";
    public static final String C_TYPE_PROYECTO_NUEVO = "PN";
    public static final String C_TYPE_INICIO_ACTIVIDAD = "IA";
    public static final String C_TYPE_FIN_ACTIVIDAD = "FA";
    public static final String C_TYPE_INTOFIN_ACTIVIDAD = "ITOF";
    public static final String C_TYPE_UPDATE_STATUS_IMPLEMENTACION = "US";
    public static final String QR_GET_PROYECTOS_PM = CONFIG_PROPERTIES.getProperty("qr_sf_get_proyectos_pm");
    public static final String QR_GET_NUEVOS_PROYECTOS_PM = CONFIG_PROPERTIES.getProperty("qr_sf_get_proyectos_nuevos");
    public static final String QR_GET_ESTATUS_OS_SF = CONFIG_PROPERTIES.getProperty("qr_sf_get_estatus_actualizados_sf");
    public static final String QR_GET_ACTIVIDADES_MES = CONFIG_PROPERTIES.getProperty("qr_ffm_get_actividades_mes");
}
