package com.reportespms.baseobjects;

import com.reportespms.utils.ConeccionSF;

public class Variables {
    private static String SF_US;
    private static String SF_PA;
    private static String SF_END_POINT;
    private static ConeccionSF CONECTION_SF;
    private static String DIAS_PLAN;
    private static String DIAS_SOLUCION;
    private static String COLOR_OK;
    private static String COLOR_M;
    private static String COLOR_B;
    private static String COLOR_SB;
    private static String DESC_IMO;
    private static String BGCOLOR_IMP;
    private static String TXTCOLOR_IMP;
    private static String DESC_ACT;
    private static String BGCOLOR_ACT;
    private static String TXTCOLOR_ACT;
    private static String DESC_PRO; 
    private static String BGCOLOR_PRO; 
    private static String TXTCOLOR_PRO;
    private static String QR_STATUS_ENL;
    private static String QR_STATUS_REC;

    public static void setSF_US(String SF_US) {
        Variables.SF_US = SF_US;
    }

    public static String getSF_US() {
        return SF_US;
    }

    public static void setSF_PA(String SF_PA) {
        Variables.SF_PA = SF_PA;
    }

    public static String getSF_PA() {
        return SF_PA;
    }

    public static void setSF_END_POINT(String SF_END_POINT) {
        Variables.SF_END_POINT = SF_END_POINT;
    }

    public static String getSF_END_POINT() {
        return SF_END_POINT;
    }

    public static void setCONECTION_SF(ConeccionSF CONECTION_SF) {
        Variables.CONECTION_SF = CONECTION_SF;
    }

    public static ConeccionSF getCONECTION_SF() {
        return CONECTION_SF;
    }

    public static void setDIAS_PLAN(String DIAS_PLAN) {
        Variables.DIAS_PLAN = DIAS_PLAN;
    }

    public static String getDIAS_PLAN() {
        return DIAS_PLAN;
    }

    public static void setDIAS_SOLUCION(String DIAS_SOLUCION) {
        Variables.DIAS_SOLUCION = DIAS_SOLUCION;
    }

    public static String getDIAS_SOLUCION() {
        return DIAS_SOLUCION;
    }

    public static void setCOLOR_OK(String COLOR_OK) {
        Variables.COLOR_OK = COLOR_OK;
    }

    public static String getCOLOR_OK() {
        return COLOR_OK;
    }

    public static void setCOLOR_M(String COLOR_M) {
        Variables.COLOR_M = COLOR_M;
    }

    public static String getCOLOR_M() {
        return COLOR_M;
    }

    public static void setCOLOR_B(String COLOR_B) {
        Variables.COLOR_B = COLOR_B;
    }

    public static String getCOLOR_B() {
        return COLOR_B;
    }

    public static void setCOLOR_SB(String COLOR_SB) {
        Variables.COLOR_SB = COLOR_SB;
    }

    public static String getCOLOR_SB() {
        return COLOR_SB;
    }

    public static void setDESC_IMO(String DESC_IMO) {
        Variables.DESC_IMO = DESC_IMO;
    }

    public static String getDESC_IMO() {
        return DESC_IMO;
    }

    public static void setBGCOLOR_IMP(String BGCOLOR_IMP) {
        Variables.BGCOLOR_IMP = BGCOLOR_IMP;
    }

    public static String getBGCOLOR_IMP() {
        return BGCOLOR_IMP;
    }

    public static void setTXTCOLOR_IMP(String TXTCOLOR_IMP) {
        Variables.TXTCOLOR_IMP = TXTCOLOR_IMP;
    }

    public static String getTXTCOLOR_IMP() {
        return TXTCOLOR_IMP;
    }

    public static void setDESC_ACT(String DESC_ACT) {
        Variables.DESC_ACT = DESC_ACT;
    }

    public static String getDESC_ACT() {
        return DESC_ACT;
    }

    public static void setBGCOLOR_ACT(String BGCOLOR_ACT) {
        Variables.BGCOLOR_ACT = BGCOLOR_ACT;
    }

    public static String getBGCOLOR_ACT() {
        return BGCOLOR_ACT;
    }

    public static void setTXTCOLOR_ACT(String TXTCOLOR_ACT) {
        Variables.TXTCOLOR_ACT = TXTCOLOR_ACT;
    }

    public static String getTXTCOLOR_ACT() {
        return TXTCOLOR_ACT;
    }

    public static void setDESC_PRO(String DESC_PRO) {
        Variables.DESC_PRO = DESC_PRO;
    }

    public static String getDESC_PRO() {
        return DESC_PRO;
    }

    public static void setBGCOLOR_PRO(String BGCOLOR_PRO) {
        Variables.BGCOLOR_PRO = BGCOLOR_PRO;
    }

    public static String getBGCOLOR_PRO() {
        return BGCOLOR_PRO;
    }

    public static void setTXTCOLOR_PRO(String TXTCOLOR_PRO) {
        Variables.TXTCOLOR_PRO = TXTCOLOR_PRO;
    }

    public static String getTXTCOLOR_PRO() {
        return TXTCOLOR_PRO;
    }

    public static void setQR_STATUS_ENL(String QR_STATUS_ENL) {
        Variables.QR_STATUS_ENL = QR_STATUS_ENL;
    }

    public static String getQR_STATUS_ENL() {
        return QR_STATUS_ENL;
    }

    public static void setQR_STATUS_REC(String QR_STATUS_REC) {
        Variables.QR_STATUS_REC = QR_STATUS_REC;
    }

    public static String getQR_STATUS_REC() {
        return QR_STATUS_REC;
    }
}
