package com.reportespms.logica;

import com.connectionsfffm.objetos.OutputsFFM;
import com.connectionsfffm.objetos.OutputsSF;

import com.reportespms.baseobjects.Constantes;
import com.reportespms.baseobjects.Variables;
import com.reportespms.inputs.InputReporteGeneral;
import com.reportespms.objects.OT;
import com.reportespms.objects.Planeacion;
import com.reportespms.objects.ReporteGeneral;

import com.reportespms.utils.LocalUtils;

import com.sforce.soap.partner.QueryResult;
import com.sforce.soap.partner.sobject.SObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jersey.repackaged.com.google.common.collect.Lists;

public class LogicaReporteGeneral {
    public LogicaReporteGeneral() {
        super();
    }
    
    
    public static Map<String,ReporteGeneral> loadFullReport(Map<String,ReporteGeneral> mapPaquetesSf,Map<String,OT> mapOTsRec,Map<String,OT> mapOTsEnl, Map<String,Planeacion> mapPlaneacionCsp){
        
        Map<String,ReporteGeneral> fullReport = new HashMap<String,ReporteGeneral>();
        
        try{
            
            for (Map.Entry<String, ReporteGeneral> planSf : mapPaquetesSf.entrySet()) {
                ReporteGeneral element_reporte = planSf.getValue();
                if(mapOTsRec.containsKey(element_reporte.getFolioOs())){
                    OT otRec = mapOTsRec.get(element_reporte.getFolioOs());
                    element_reporte.setIdOt(otRec.getIdOt());
                    element_reporte.setTipoIntervencion(otRec.getIdTipoIntervencion());
                    element_reporte.setSubTipoIntervencion(otRec.getId_SubTipoIntervencion());
                    element_reporte.setEstatusOt(otRec.getDesEstatusOt());
                    element_reporte.setEstadoOt(otRec.getDesEstadoOt());
                    element_reporte.setMotivoOt(otRec.getDesMotivoOt());
                    element_reporte.setFechaAgendamiento(otRec.getFechaAgendaOt());
                    element_reporte.setFechaInstalacion(otRec.getFechaTerminoOt());
                    element_reporte.setSemanaAgenda(LocalUtils.calculaNumeroSemana(otRec.getFechaAgendaOt()));
                    element_reporte.setSemanaInstalacion(LocalUtils.calculaNumeroSemana(otRec.getFechaTerminoOt()));
                    element_reporte.setTiempoVentaToInstalacion(LocalUtils.tiempoEntreFechas(element_reporte.getFechaVenta(), otRec.getFechaTerminoOt()));
                    element_reporte.setTiempoGanadaToInstalacion(LocalUtils.tiempoEntreFechas(element_reporte.getFechaCerradaGanada(), otRec.getFechaTerminoOt()));
                }else if(mapOTsEnl.containsKey(element_reporte.getFolioOs())){
                    OT otEnl = mapOTsEnl.get(element_reporte.getFolioOs());
                    element_reporte.setIdOt(otEnl.getIdOt());
                    element_reporte.setTipoIntervencion(otEnl.getIdTipoIntervencion());
                    element_reporte.setSubTipoIntervencion(otEnl.getId_SubTipoIntervencion());
                    element_reporte.setEstatusOt(otEnl.getDesEstatusOt());
                    element_reporte.setEstadoOt(otEnl.getDesEstadoOt());
                    element_reporte.setMotivoOt(otEnl.getDesMotivoOt());
                    element_reporte.setFechaAgendamiento(otEnl.getFechaAgendaOt());
                    element_reporte.setFechaInstalacion(otEnl.getFechaTerminoOt());
                    element_reporte.setSemanaAgenda(LocalUtils.calculaNumeroSemana(otEnl.getFechaAgendaOt()));
                    element_reporte.setSemanaInstalacion(LocalUtils.calculaNumeroSemana(otEnl.getFechaTerminoOt()));
                    element_reporte.setTiempoVentaToInstalacion(LocalUtils.tiempoEntreFechas(element_reporte.getFechaVenta(), otEnl.getFechaTerminoOt()));
                    element_reporte.setTiempoGanadaToInstalacion(LocalUtils.tiempoEntreFechas(element_reporte.getFechaCerradaGanada(), otEnl.getFechaTerminoOt()));
                }else{
                    element_reporte.setIdOt("SIN OT");
                    element_reporte.setTipoIntervencion("NA");
                    element_reporte.setSubTipoIntervencion("NA");
                    element_reporte.setEstatusOt("NA");
                    element_reporte.setEstadoOt("NA");
                    element_reporte.setMotivoOt("NA");
                    element_reporte.setFechaAgendamiento("NA");
                    element_reporte.setFechaInstalacion("NA");
                    element_reporte.setSemanaAgenda("NA");
                    element_reporte.setSemanaInstalacion("NA");
                    element_reporte.setTiempoVentaToInstalacion("NA");
                    element_reporte.setTiempoGanadaToInstalacion("NA");        
                }
                
                if(mapPlaneacionCsp.containsKey(element_reporte.getIdCotSitioPlan())){
                    Planeacion pl = mapPlaneacionCsp.get(element_reporte.getIdCotSitioPlan());
                    element_reporte.setSemaforoDesviacion(LocalUtils.semaforoDesvicacion(element_reporte.getFechaCerradaGanada(),pl.getFechaComprometida(), element_reporte.getTipoplan()));
                    element_reporte.setEstatusPlaneacion(pl.getEstatusPlaneacion());
                    element_reporte.setFechaComprometida(pl.getFechaComprometida());
                    element_reporte.setNumeroEmpleadoResponsable(pl.getNumEmpleadoResponsable());
                    element_reporte.setNombreResponsable(pl.getNombreResponsable());
                    element_reporte.setComentarios(pl.getComentarios());
                    element_reporte.setDesviacionDias(pl.getDesviacionDias());
                }else{
                    element_reporte.setFechaObjetivo("NA");
                    element_reporte.setSemanaObjetivo("NA");
                    element_reporte.setSemaforoDesviacion("NA");
                    element_reporte.setEstatusPlaneacion("NA");
                    element_reporte.setFechaComprometida("NA");
                    element_reporte.setNumeroEmpleadoResponsable("NA");
                    element_reporte.setNombreResponsable("NA");
                    element_reporte.setComentarios("NA");
                    element_reporte.setDesviacionDias("NA");
                }
                element_reporte.setFolioOs(element_reporte.getFolioOs().length()<3 ? "SIN OS" : element_reporte.getFolioOs());
                fullReport.put(element_reporte.getFolioCsp(), element_reporte);
            }
            
        }catch(Exception ex){
            LocalUtils.console(Constantes.C_TYPE_GEN_ERR, "Error ex : " + ex.toString(),"loadFullReport");    
        }
        
        return fullReport;
                
    }
    
    public static Map<String,ReporteGeneral> loadDataSF(InputReporteGeneral input_ws){
        
        Map<String,ReporteGeneral> map_dataSF = new HashMap<String,ReporteGeneral>();
        
        try{
            
            Map<String,String> map_fechas = LocalUtils.fechasFiltroSf(input_ws.getTipoFiltro(), input_ws.getFecha());
            
            OutputsSF out_conector_sf = Variables.getCONECTION_SF().consultaSalesforce(Constantes.C_SF_REPORTE_GENERAL.replaceAll("idPm", input_ws.getIdPm()).replaceAll("fechaIn", map_fechas.get("fechaIn")).replaceAll("fechaFin", map_fechas.get("fechaFin"))); 
            
            if(out_conector_sf.getResult()){
                QueryResult planes_sf = out_conector_sf.getQueryResult();
                if(planes_sf.getDone()){
                    if(planes_sf.getSize() > 0){
                        int cont = 0;
                        for(SObject plan_sf : planes_sf.getRecords()){
                            ReporteGeneral element_reporte = new ReporteGeneral();
                            element_reporte.setIdCotSitioPlan((String) plan_sf.getField("Id"));
                            element_reporte.setFolioCsp((String) plan_sf.getField("Name"));
                            element_reporte.setCuentaFactura((String) plan_sf.getField("CuentaFacturaNumero__c"));
                            element_reporte.setPlan((String) plan_sf.getField("NombrePlan__c"));
                            element_reporte.setPrecioRenta(LocalUtils.formateaMontos((String) plan_sf.getField("TotalRenta__c")));
                            element_reporte.setPrecioRentaConInpuestos(LocalUtils.formateaMontos((String) plan_sf.getField("TotalRenta_ConImpuesto__c")));
                            element_reporte.setTipoCuadrilla(plan_sf.getField("CuadrillaFFM__c") != null ? (String) plan_sf.getField("CuadrillaFFM__c") : "Empresarial");
                            element_reporte.setFechaActivacion(LocalUtils.getDateSF((String) plan_sf.getField("FechaAprovisionamiento__c")));
                            
                            if(Constantes.JAR_UTILS.validateDataSF(plan_sf, "DP_Plan__r")){
                                SObject plan = (SObject) plan_sf.getSObjectField("DP_Plan__r"); 
                                element_reporte.setTipoplan(plan.getField("SM__c").equals("true") ? Constantes.C_LABEL_SOLUCION : Constantes.C_LABEL_PAQUETE);
                            }
                            
                            if(Constantes.JAR_UTILS.validateDataSF(plan_sf, "Cotizacion__r")){
                                SObject cot_sf = (SObject) plan_sf.getSObjectField("Cotizacion__r"); 
                                element_reporte.setFolioCotizacion((String) cot_sf.getField("Name"));
                                element_reporte.setGerencia((String) cot_sf.getField("Segmento_Gerencia__c"));
                                element_reporte.setTopCincomil((String) cot_sf.getField("Top5mil__c"));
                                element_reporte.setTipoCotizacion((String) cot_sf.getField("TipoCotizacion__c"));
                                if(Constantes.JAR_UTILS.validateDataSF(cot_sf, "Oportunidad__r")){
                                    SObject oportunidad_sf = (SObject) cot_sf.getSObjectField("Oportunidad__r");
                                    element_reporte.setNombreOportunidad((String) oportunidad_sf.getField("Name"));                
                                    element_reporte.setNumeroOportunidad((String) oportunidad_sf.getField("NumeroOportunidad__c"));
                                    element_reporte.setTipoVenta((String) oportunidad_sf.getField("Type"));                        
                                    element_reporte.setPlazaVendedor((String) oportunidad_sf.getField("PlazaVenta__c"));           
                                    element_reporte.setSegmento((String) oportunidad_sf.getField("Segmento__c"));                  
                                    element_reporte.setFechaVenta(LocalUtils.getDateSF((String) oportunidad_sf.getField("TsFirma__c")));                 
                                    element_reporte.setFechaCerradaGanada(LocalUtils.getDateSFSimple((String) oportunidad_sf.getField("CloseDate")));
                                    element_reporte.setGerencia(element_reporte.getGerencia()!= null && element_reporte.getSegmento() != null ? element_reporte.getGerencia().replaceAll(element_reporte.getSegmento(), "").trim() : element_reporte.getGerencia());
                                    if(Constantes.JAR_UTILS.validateDataSF(oportunidad_sf, "Account")){
                                        SObject cuenta_sf = (SObject) oportunidad_sf.getSObjectField("Account");
                                        element_reporte.setCliente((String) cuenta_sf.getField("RazonSocial__c"));
                                    }
                                    if(Constantes.JAR_UTILS.validateDataSF(oportunidad_sf, "Owner")){
                                        SObject vendedor_sf = (SObject) oportunidad_sf.getSObjectField("Owner");
                                        element_reporte.setNombreVendedor((String) vendedor_sf.getField("Name"));
                                    }
                                }
                                if(Constantes.JAR_UTILS.validateDataSF(cot_sf, "AsignadoPM__r")){
                                    SObject pm_sf = (SObject) cot_sf.getSObjectField("AsignadoPM__r");
                                    element_reporte.setNombrePm((String) pm_sf.getField("Name"));
                                }
                            }
                            
                            if(Constantes.JAR_UTILS.validateDataSF(plan_sf, "Cot_Sitio__r")){
                                SObject cs_sf = (SObject) plan_sf.getSObjectField("Cot_Sitio__r");
                                element_reporte.setIdCotSitio((String) cs_sf.getField("Id"));
                                element_reporte.setFolioCotSitio((String) cs_sf.getField("Name"));
                                element_reporte.setDireccionSitio((String) cs_sf.getField("DireccionSitio__c"));
                                
                                if(Constantes.JAR_UTILS.validateDataSF(cs_sf, "Sitio__r")){
                                    SObject sitio_sf = (SObject) cs_sf.getSObjectField("Sitio__r"); 
                                    element_reporte.setNombreSitio((String) sitio_sf.getField("IdSitio__c"));
                                    element_reporte.setRegion((String) sitio_sf.getField("Region__c"));      
                                    element_reporte.setCiudad((String) sitio_sf.getField("Ciudad__c"));      
                                    element_reporte.setDistrito((String) sitio_sf.getField("Distrito2__c")); 
                                    element_reporte.setCluster((String) sitio_sf.getField("Clouster2__c"));  
                                }
                            }
                            
                            if(Constantes.JAR_UTILS.validateDataSF(plan_sf, "OrdenServicio__r")){
                                SObject os_sf = (SObject) plan_sf.getSObjectField("OrdenServicio__r"); 
                                element_reporte.setFolioOs((String) os_sf.getField("Name"));
                                element_reporte.setEstatatusOs((String) os_sf.getField("Estatus__c"));
                            }else{
                                element_reporte.setFolioOs("-1");
                                element_reporte.setEstatatusOs("Pendiente");
                            }
                            
                            element_reporte.setFechaObjetivo(LocalUtils.getfechaObjectivo(element_reporte.getFechaCerradaGanada(), element_reporte.getTipoplan()));
                            element_reporte.setSemanaObjetivo(LocalUtils.calculaNumeroSemana(LocalUtils.getfechaObjectivo(element_reporte.getFechaCerradaGanada(), element_reporte.getTipoplan())));
                            element_reporte.setSemanaVenta(LocalUtils.calculaNumeroSemana(element_reporte.getFechaVenta()));
                            element_reporte.setSemanaCerradaGanada(LocalUtils.calculaNumeroSemana(element_reporte.getFechaCerradaGanada()));
                            element_reporte.setEstatusCsp(LocalUtils.estatusCsp(element_reporte.getEstatatusOs()));
                            
                            map_dataSF.put((String) plan_sf.getField("Name"), element_reporte);
                        }
                    }
                }
            }else{
                LocalUtils.console(Constantes.C_TYPE_ERR_SF, out_conector_sf.getResultDescripcion(), "loadDataSF");    
            }
        
        }catch(Exception ex){
            LocalUtils.console(Constantes.C_TYPE_GEN_ERR, "Error ex : " + ex.toString(),"loadDataSF");    
        }
        
        return map_dataSF;
           
    }
    
    public static Map<String,OT> loadOTsEnl(List<List<String>> listaOsEnl){
        
        Map<String,OT> map_ots = new HashMap<String,OT>();
        
        try{
            
            for(List<String> bloque : listaOsEnl){
                String base_qr = "";
                ArrayList<String> arrOSs = new ArrayList<String>();
                
                for(String elemneto : bloque){
                    arrOSs.add(elemneto);
                }
                
                base_qr = arrOSs.toString().replace("[", "'").replace("]", "'").replaceAll(", ", "','"); 
                
                System.err.println("Consulta FFM : " + Constantes.C_OTS_ENL_REPORTE_GENERAL.replaceAll("folios_os", base_qr));
                
                OutputsFFM out_conec_ffm = Constantes.CONECTION_FFM.consultaBD(Constantes.C_OTS_ENL_REPORTE_GENERAL.replaceAll("folios_os", base_qr));
                
                if(out_conec_ffm.getResult()){
                    String ots [][] = out_conec_ffm.getArr_records();
                    for(int fila = 0; fila < ots.length; fila++){
                        OT ot_enl = new OT();
                        ot_enl.setIdOt(ots[fila][0]);
                        ot_enl.setIdTipoIntervencion(ots[fila][1]);
                        ot_enl.setId_SubTipoIntervencion(ots[fila][2]);
                        ot_enl.setDesEstatusOt(ots[fila][3]);
                        ot_enl.setDesEstadoOt(ots[fila][4]);
                        ot_enl.setDesMotivoOt(ots[fila][5]);
                        ot_enl.setFechaPrimerAgendaOt(ots[fila][6]);
                        ot_enl.setFechaAgendaOt(ots[fila][7]);
                        ot_enl.setFechaTerminoOt(ots[fila][9]);
                        map_ots.put(ots[fila][8], ot_enl);
                    }
                }
            }
            
        }catch(Exception ex){
            LocalUtils.console(Constantes.C_TYPE_GEN_ERR, "Error : " + ex.toString(),  "loadOTsEnl");    
        }        
        
        return map_ots;
    }
    
    public static Map<String,OT> loadOTsRec(List<List<String>> listaOsRec){
        
        Map<String,OT> map_ots = new HashMap<String,OT>();
        
        try{
            for(List<String> bloque : listaOsRec){
                String base_qr = "";
                ArrayList<String> arrOSs = new ArrayList<String>();
                
                for(String elemneto : bloque){
                    arrOSs.add(elemneto);
                }
                
                base_qr = arrOSs.toString().replace("[", "'").replace("]", "'").replaceAll(", ", "','"); 
                
                System.err.println("Consulta FFM : " + Constantes.C_OTS_REC_REPORTE_GENERAL.replaceAll("folios_os", base_qr));
                
                OutputsFFM out_conec_ffm = Constantes.CONECTION_FFM.consultaBD(Constantes.C_OTS_REC_REPORTE_GENERAL.replaceAll("folios_os", base_qr));
                
                if(out_conec_ffm.getResult()){
                    String ots [][] = out_conec_ffm.getArr_records();
                    for(int fila = 0; fila < ots.length; fila++){
                        OT ot_enl = new OT();
                        ot_enl.setIdOt(ots[fila][0]);
                        ot_enl.setIdTipoIntervencion(ots[fila][1]);
                        ot_enl.setId_SubTipoIntervencion(ots[fila][2]);
                        ot_enl.setDesEstatusOt(ots[fila][3]);
                        ot_enl.setDesEstadoOt(ots[fila][4]);
                        ot_enl.setDesMotivoOt(ots[fila][5]);
                        ot_enl.setFechaPrimerAgendaOt(ots[fila][6]);
                        ot_enl.setFechaAgendaOt(ots[fila][7]);
                        ot_enl.setFechaTerminoOt(ots[fila][9]);
                        System.err.println("dd _ " + ots[fila][8]);
                        map_ots.put(ots[fila][8], ot_enl);
                    }
                }
            }
        }catch(Exception ex){
            LocalUtils.console(Constantes.C_TYPE_GEN_ERR, "Error : " + ex.toString(),  "loadOTsEnl");    
        }        
        
        return map_ots;
    }
    
    public static Map<String,Planeacion> loadPlaneacionCsp(List<List<String>> listaCsps){
        
        Map<String,Planeacion> mapPlaneacion = new HashMap<String,Planeacion>();
        
        try{
            for(List<String> bloque : listaCsps){
                String base_qr = "";
                ArrayList<String> arrCsps = new ArrayList<String>();
                
                for(String elemneto : bloque){
                    arrCsps.add(elemneto);
                }
                
                base_qr = arrCsps.toString().replace("[", "'").replace("]", "'").replaceAll(", ", "','"); 
                
                System.err.println("Consulta FFM : " + Constantes.C_DATOS_PLANEACION_GENRAL.replaceAll("idsPuenteSf", base_qr));
                
                OutputsFFM out_conec_ffm = Constantes.CONECTION_FFM.consultaBD(Constantes.C_DATOS_PLANEACION_GENRAL.replaceAll("idsPuenteSf", base_qr));
                
                if(out_conec_ffm.getResult()){
                    String planes [][] = out_conec_ffm.getArr_records();
                    for(int fila = 0; fila < planes.length; fila++){
                        Planeacion infoPlan = new Planeacion();
                        infoPlan.setEstatusPlaneacion(planes[fila][1]);
                        infoPlan.setFechaComprometida(planes[fila][2]);
                        infoPlan.setNumEmpleadoResponsable(planes[fila][3]);
                        infoPlan.setNombreResponsable(planes[fila][4]);
                        infoPlan.setComentarios(planes[fila][5]);
                        infoPlan.setDesviacionDias(LocalUtils.tiempoTranscurrido(planes[fila][2]));
                        infoPlan.setSemaforoDesviacion(LocalUtils.tiempoTranscurrido(planes[fila][2]));
                        mapPlaneacion.put(planes[fila][0], infoPlan);
                    }
                }
            }
        }catch(Exception ex){
            LocalUtils.console(Constantes.C_TYPE_GEN_ERR, "Error : " + ex.toString(),  "loadOTsEnl");    
        }        
        
        return mapPlaneacion;
    }
    
    
    
    
    public static List<List<String>> getListOSsEnl(Map<String,ReporteGeneral> map_info_plan){
        
        List<List<String>> listaOs = null;
        
        try{
            ArrayList<String> listOss = new ArrayList<String>();
            for (Map.Entry<String, ReporteGeneral> entry : map_info_plan.entrySet()) {
                ReporteGeneral oss =entry.getValue();
                if(!oss.getFolioOs().equals("-1")){
                    if(oss.getTipoCuadrilla().equals("Empresarial") || oss.getTipoCuadrilla().equals("")){
                        listOss.add(oss.getFolioOs());        
                    }
                }
            }
            
            listaOs = Lists.partition(listOss, 850);    
            
        }catch(Exception ex){
            LocalUtils.console(Constantes.C_TYPE_GEN_ERR, "Error : " + ex.toString(), "getListOSsEnl");
        }    
        
        return listaOs;
    }
    
    public static List<List<String>> getListOSsRec(Map<String,ReporteGeneral> map_info_plan){
        
        List<List<String>> listaOs = null;
        
        try{
            ArrayList<String> listOss = new ArrayList<String>();
            for (Map.Entry<String, ReporteGeneral> entry : map_info_plan.entrySet()) {
                ReporteGeneral oss = entry.getValue();
                if(!oss.getFolioOs().equals("-1")){
                    if(oss.getTipoCuadrilla() != null && !oss.getTipoCuadrilla().equals("") && !oss.getTipoCuadrilla().equals("Empresarial") ){
                        listOss.add(oss.getFolioOs());    
                    }
                }
            }
            
            listaOs = Lists.partition(listOss, 850);
            
        }catch(Exception ex){
            LocalUtils.console(Constantes.C_TYPE_GEN_ERR, "Error : " + ex.toString(), "getListOSsRec");
        }    
        
        return listaOs;
    }
    
    public static List<List<String>> getListCsp(Map<String,ReporteGeneral> map_info_plan){
        List<List<String>> listaCs = null;
        
        try{
            ArrayList<String> listCs = new ArrayList<String>();
            for (Map.Entry<String, ReporteGeneral> entry : map_info_plan.entrySet()) {
                ReporteGeneral oss =entry.getValue();
                listCs.add(oss.getIdCotSitioPlan());    
            }
            
            listaCs = Lists.partition(listCs, 850);
            
        }catch(Exception ex){
            LocalUtils.console(Constantes.C_TYPE_GEN_ERR, "Error : " + ex.toString(), "getListOSs");
        }    
        
        return listaCs;
    }
}
