package com.reportespms.logica;

import com.connectionsfffm.objetos.OutputsSF;

import com.reportespms.baseobjects.Constantes;
import com.reportespms.baseobjects.Variables;
import com.reportespms.inputs.InputGraficaReporteGeneralPMs;
import com.reportespms.objects.ElementoGrafica;
import com.reportespms.objects.GraficaPM;
import com.reportespms.objects.ReporteGeneral;
import com.reportespms.utils.LocalUtils;

import com.sforce.soap.partner.QueryResult;
import com.sforce.soap.partner.sobject.SObject;

import java.util.HashMap;
import java.util.Map;

public class LogicaGraficaReporteGeneral {
    public LogicaGraficaReporteGeneral() {
        super();
    }
    
    
    public static Map<String,ReporteGeneral> loadDataSF(InputGraficaReporteGeneralPMs input_ws,String baseQr){
        
        Map<String,ReporteGeneral> map_dataSF = new HashMap<String,ReporteGeneral>();
        
        try{
            
            Map<String,String> map_fechas = LocalUtils.fechasFiltroSf("M", input_ws.getFecha());
            
            OutputsSF out_conector_sf = Variables.getCONECTION_SF().consultaSalesforce(baseQr.replaceAll("idPm", input_ws.getIdPm()).replaceAll("fechaIn", map_fechas.get("fechaIn")).replaceAll("fechaFin", map_fechas.get("fechaFin"))); 
            
            if(out_conector_sf.getResult()){
                QueryResult planes_sf = out_conector_sf.getQueryResult();
                if(planes_sf.getDone()){
                    if(planes_sf.getSize() > 0){
                        int cont = 0;
                        for(SObject plan_sf : planes_sf.getRecords()){
                            ReporteGeneral element_reporte = new ReporteGeneral();
                            element_reporte.setFolioCsp((String) plan_sf.getField("Name"));
                            element_reporte.setCuentaFactura((String) plan_sf.getField("CuentaFacturaNumero__c"));
                            element_reporte.setPlan((String) plan_sf.getField("NombrePlan__c"));
                            element_reporte.setPrecioRenta(LocalUtils.formateaMontos((String) plan_sf.getField("TotalRenta__c")));
                            element_reporte.setPrecioRentaConInpuestos(LocalUtils.formateaMontos((String) plan_sf.getField("TotalRenta_ConImpuesto__c")));
                            element_reporte.setTipoCuadrilla(plan_sf.getField("CuadrillaFFM__c") != null ? (String) plan_sf.getField("CuadrillaFFM__c") : "Empresarial");
                            element_reporte.setFechaActivacion(LocalUtils.getDateSF((String) plan_sf.getField("FechaAprovisionamiento__c")));
                            
                            if(Constantes.JAR_UTILS.validateDataSF(plan_sf, "DP_Plan__r")){
                                SObject plan = (SObject) plan_sf.getSObjectField("DP_Plan__r"); 
                                element_reporte.setTipoplan(plan.getField("SM__c").equals("true") ? Constantes.C_LABEL_SOLUCION : Constantes.C_LABEL_PAQUETE);
                            }
                            
                            if(Constantes.JAR_UTILS.validateDataSF(plan_sf, "Cotizacion__r")){
                                SObject cot_sf = (SObject) plan_sf.getSObjectField("Cotizacion__r"); 
                                element_reporte.setFolioCotizacion((String) cot_sf.getField("Name"));
                                element_reporte.setTopCincomil((String) cot_sf.getField("Top5mil__c"));
                                if(Constantes.JAR_UTILS.validateDataSF(cot_sf, "Oportunidad__r")){
                                    SObject oportunidad_sf = (SObject) cot_sf.getSObjectField("Oportunidad__r");
                                    element_reporte.setNumeroOportunidad((String) oportunidad_sf.getField("NumeroOportunidad__c"));
                                    element_reporte.setTipoVenta((String) oportunidad_sf.getField("Type"));                        
                                    element_reporte.setPlazaVendedor((String) oportunidad_sf.getField("PlazaVenta__c"));           
                                    element_reporte.setSegmento((String) oportunidad_sf.getField("Segmento__c"));                  
                                    element_reporte.setFechaVenta(LocalUtils.getDateSF((String) oportunidad_sf.getField("TsFirma__c")));                 
                                    element_reporte.setFechaCerradaGanada(LocalUtils.getDateSFSimple((String) oportunidad_sf.getField("CloseDate")));
                                    element_reporte.setGerencia(element_reporte.getGerencia()!= null && element_reporte.getSegmento() != null ? element_reporte.getGerencia().replaceAll(element_reporte.getSegmento(), "").trim() : element_reporte.getGerencia());
                                    if(Constantes.JAR_UTILS.validateDataSF(oportunidad_sf, "Account")){
                                        SObject cuenta_sf = (SObject) oportunidad_sf.getSObjectField("Account");
                                        element_reporte.setCliente((String) cuenta_sf.getField("RazonSocial__c"));
                                    }
                                }
                            }
                            
                            if(Constantes.JAR_UTILS.validateDataSF(plan_sf, "Cot_Sitio__r")){
                                SObject cs_sf = (SObject) plan_sf.getSObjectField("Cot_Sitio__r");
                                element_reporte.setFolioCotSitio((String) cs_sf.getField("Name"));
                                element_reporte.setDireccionSitio((String) cs_sf.getField("DireccionSitio__c"));
                            }
                            
                            if(Constantes.JAR_UTILS.validateDataSF(plan_sf, "OrdenServicio__r")){
                                SObject os_sf = (SObject) plan_sf.getSObjectField("OrdenServicio__r"); 
                                element_reporte.setFolioOs((String) os_sf.getField("Name"));
                                element_reporte.setEstatatusOs((String) os_sf.getField("Estatus__c"));
                            }else{
                                element_reporte.setFolioOs("SIN OS");
                                element_reporte.setEstatatusOs("Pendiente");
                            }
                            
                            element_reporte.setSemanaVenta(LocalUtils.calculaNumeroSemana(element_reporte.getFechaVenta()));
                            element_reporte.setSemanaCerradaGanada(LocalUtils.calculaNumeroSemana(element_reporte.getFechaCerradaGanada()));
                            element_reporte.setFechaObjetivo(LocalUtils.getfechaObjectivo(element_reporte.getFechaCerradaGanada(), element_reporte.getTipoplan()));
                            element_reporte.setSemanaObjetivo(LocalUtils.calculaNumeroSemana(LocalUtils.getfechaObjectivo(element_reporte.getFechaCerradaGanada(), element_reporte.getTipoplan())));
                            element_reporte.setSemaforoDesviacion(LocalUtils.semaforoDesvicacion(element_reporte.getFechaCerradaGanada(),element_reporte.getFechaActivacion(), element_reporte.getTipoplan()));
                            element_reporte.setEstatusCsp(LocalUtils.estatusCsp(element_reporte.getEstatatusOs()));
                            map_dataSF.put((String) plan_sf.getField("Name"), element_reporte);
                        }
                    }
                }
            }else{
                LocalUtils.console(Constantes.C_TYPE_ERR_SF, out_conector_sf.getResultDescripcion(), "loadDataSF");    
            }
        
        }catch(Exception ex){
            LocalUtils.console(Constantes.C_TYPE_GEN_ERR, "Error ex : " + ex.toString(),"loadDataSF");    
        }
        
        return map_dataSF;
           
    }
    
    public static GraficaPM calculaGraficaGeneral(Map<String,ReporteGeneral> mapReporteGeneralActual,Map<String,ReporteGeneral> mapReporteGeneralPasado){
        
        GraficaPM detalleGrafica = new GraficaPM();
        
        Integer totalPuntas = 0;
        Float totalRenta = 0.0f;
        Integer totalPuntasPorInstalar = 0;
        Float totalRentaPorInstalar = 0.0f;
        Integer totalPuntasCalendarizado = 0;
        Float totalRentaCalendarizado = 0.0f;
        Integer totalPuntasDetenido = 0;
        Float totalRentaDetenido = 0.0f;
        Integer totalPuntasDevueltoVentas = 0;
        Float totalRentaDevueltoVentas = 0.0f;
        Integer totalPuntasCancelado = 0;
        Float totalRentaCancelado = 0.0f;
        Integer totalPuntasInstalado = 0;
        Float totalRentaInstalado = 0.0f;
        
        Integer contadorOKPorIn = 0, contadorMDPorIn = 0, contadorMAPorIn = 0, contadorDVPorIn = 0;
        Integer contadorOKCal = 0, contadorMDCal = 0, contadorMACal = 0, contadorDVCal = 0;
        Integer contadorOKDet = 0, contadorMDDet = 0, contadorMADet = 0, contadorDVDet = 0;
        Integer contadorOKVen = 0, contadorMDVen = 0, contadorMAVen = 0, contadorDVVen = 0;
        Integer contadorOKCa = 0, contadorMDCa = 0, contadorMACa = 0, contadorDVCa = 0;
        Integer contadorOKIns = 0, contadorMDIns = 0, contadorMAIns = 0, contadorDVIns = 0;
        
        //-- Grafica anterior
        Float totalRentaInstaladoMA = 0.0f;
        Integer totalPuntasInstaladasMA = 0;
        Float totalRentaBackMA = 0.0f;
        Integer totalPuntasBackMA = 0;
        Integer PorInstalarMA = 0;
        Integer CalendarizadoMA = 0;
        Integer DetenidoMA = 0;
        Integer DevueltoVentasMA = 0;
        Integer CanceladoMA = 0;
                
        try{
            totalPuntas = mapReporteGeneralActual.size();
            for(Map.Entry<String, ReporteGeneral> reporteSf : mapReporteGeneralActual.entrySet()){
                ReporteGeneral elemReporte = reporteSf.getValue();
                switch(elemReporte.getEstatusCsp()){
                    case Constantes.C_POR_INSTALAR:
                        totalPuntasPorInstalar++;
                        totalRentaPorInstalar += Float.valueOf(elemReporte.getPrecioRenta());
                        totalRenta += Float.valueOf(elemReporte.getPrecioRenta());
                        if(elemReporte.getSemaforoDesviacion().equals(Variables.getCOLOR_OK())){
                            contadorOKPorIn++;
                        }else if(elemReporte.getSemaforoDesviacion().equals(Variables.getCOLOR_M())){
                            contadorMDPorIn++;
                        }else if(elemReporte.getSemaforoDesviacion().equals(Variables.getCOLOR_B())){
                            contadorMAPorIn++;
                        }else{
                            contadorDVPorIn++;
                        }
                    break;
                    case Constantes.C_CALENDARIZADO:
                        totalPuntasCalendarizado++;
                        totalRentaCalendarizado += Float.valueOf(elemReporte.getPrecioRenta());
                        totalRenta += Float.valueOf(elemReporte.getPrecioRenta());
                        if(elemReporte.getSemaforoDesviacion().equals(Variables.getCOLOR_OK())){
                            contadorOKCal++;
                        }else if(elemReporte.getSemaforoDesviacion().equals(Variables.getCOLOR_M())){
                            contadorMDCal++;
                        }else if(elemReporte.getSemaforoDesviacion().equals(Variables.getCOLOR_B())){
                            contadorMACal++;
                        }else{
                            contadorDVCal++;
                        }
                    break;
                    case Constantes.C_DETENIDO:
                        totalPuntasDetenido++;
                        totalRentaDetenido += Float.valueOf(elemReporte.getPrecioRenta());
                        totalRenta += Float.valueOf(elemReporte.getPrecioRenta());
                        if(elemReporte.getSemaforoDesviacion().equals(Variables.getCOLOR_OK())){
                            contadorOKDet++;
                        }else if(elemReporte.getSemaforoDesviacion().equals(Variables.getCOLOR_M())){
                            contadorMDDet++;
                        }else if(elemReporte.getSemaforoDesviacion().equals(Variables.getCOLOR_B())){
                            contadorMADet++;
                        }else{
                            contadorDVDet++;
                        }
                    break;
                    case Constantes.C_DEVUELTO_VENTAS:
                        totalPuntasDevueltoVentas++;
                        totalRentaDevueltoVentas += Float.valueOf(elemReporte.getPrecioRenta());
                        totalRenta += Float.valueOf(elemReporte.getPrecioRenta());
                        if(elemReporte.getSemaforoDesviacion().equals(Variables.getCOLOR_OK())){
                            contadorOKVen++;
                        }else if(elemReporte.getSemaforoDesviacion().equals(Variables.getCOLOR_M())){
                            contadorMDVen++;
                        }else if(elemReporte.getSemaforoDesviacion().equals(Variables.getCOLOR_B())){
                            contadorMAVen++;
                        }else{
                            contadorDVVen++;
                        }
                    break;
                    case Constantes.C_CANCELADO:
                        totalPuntasCancelado++;
                        totalRentaCancelado += Float.valueOf(elemReporte.getPrecioRenta());
                        totalRenta += Float.valueOf(elemReporte.getPrecioRenta());
                        if(elemReporte.getSemaforoDesviacion().equals(Variables.getCOLOR_OK())){
                            contadorOKCa++;
                        }else if(elemReporte.getSemaforoDesviacion().equals(Variables.getCOLOR_M())){
                            contadorMDCa++;
                        }else if(elemReporte.getSemaforoDesviacion().equals(Variables.getCOLOR_B())){
                            contadorMACa++;
                        }else{
                            contadorDVCa++;
                        }
                    break;
                    case Constantes.C_INSTALADO:
                        totalPuntasInstalado++;
                        totalRentaInstalado += Float.valueOf(elemReporte.getPrecioRenta());
                        totalRenta += Float.valueOf(elemReporte.getPrecioRenta());
                        if(elemReporte.getSemaforoDesviacion().equals(Variables.getCOLOR_OK())){
                            contadorOKIns++;
                        }else if(elemReporte.getSemaforoDesviacion().equals(Variables.getCOLOR_M())){
                            contadorMDIns++;
                        }else if(elemReporte.getSemaforoDesviacion().equals(Variables.getCOLOR_B())){
                            contadorMAIns++;
                        }else{
                            contadorDVIns++;
                        }
                    break;
                }
                
                
            }
            
            
            for(Map.Entry<String, ReporteGeneral> reporteSf : mapReporteGeneralPasado.entrySet()){
                ReporteGeneral elemReporte = reporteSf.getValue();
                switch(elemReporte.getEstatusCsp()){
                    case Constantes.C_POR_INSTALAR:
                        PorInstalarMA++;
                        totalPuntasBackMA++;
                        totalRentaBackMA += Float.valueOf(elemReporte.getPrecioRenta());
                    break;
                    case Constantes.C_CALENDARIZADO:
                        CalendarizadoMA++;
                        totalPuntasBackMA++;
                        totalRentaBackMA += Float.valueOf(elemReporte.getPrecioRenta());
                    break;
                    case Constantes.C_DETENIDO:
                        DetenidoMA++;
                        totalPuntasBackMA++;
                        totalRentaBackMA += Float.valueOf(elemReporte.getPrecioRenta());
                    break;
                    case Constantes.C_DEVUELTO_VENTAS:
                        DevueltoVentasMA++;
                        totalPuntasBackMA++;
                        totalRentaBackMA += Float.valueOf(elemReporte.getPrecioRenta());
                    break;
                    case Constantes.C_CANCELADO:
                        CanceladoMA++;
                        totalPuntasBackMA++;
                        totalRentaBackMA += Float.valueOf(elemReporte.getPrecioRenta());
                    break;
                    case Constantes.C_INSTALADO:
                        totalPuntasInstaladasMA++;
                        totalRentaInstaladoMA += Float.valueOf(elemReporte.getPrecioRenta());
                    break;
                }
            }
            
            ElementoGrafica arrElementosGrafica [] = new ElementoGrafica[6];
            arrElementosGrafica[0] = loadElementosGrafica(Constantes.C_POR_INSTALAR, contadorOKPorIn, contadorMDPorIn, contadorMAPorIn, contadorDVPorIn);
            arrElementosGrafica[1] = loadElementosGrafica(Constantes.C_CALENDARIZADO, contadorOKCal, contadorMDCal, contadorMACal, contadorDVCal);
            arrElementosGrafica[2] = loadElementosGrafica(Constantes.C_DETENIDO, contadorOKDet, contadorMDDet, contadorMADet, contadorDVDet);
            arrElementosGrafica[3] = loadElementosGrafica(Constantes.C_DEVUELTO_VENTAS, contadorOKVen, contadorMDVen, contadorMAVen, contadorDVVen);
            arrElementosGrafica[4] = loadElementosGrafica(Constantes.C_CANCELADO, contadorOKCa, contadorMDCa, contadorMACa, contadorDVCa);
            arrElementosGrafica[5] = loadElementosGrafica(Constantes.C_INSTALADO, contadorOKIns, contadorMDIns, contadorMAIns, contadorDVIns);
                
            detalleGrafica.setTotalPuntas(totalPuntas.toString());
            detalleGrafica.setTotalRenta(totalRenta.toString());
            detalleGrafica.setTotalPuntasPorInstalar(totalPuntasPorInstalar.toString());
            detalleGrafica.setTotalRentaPorInstalar(totalRentaPorInstalar.toString());
            detalleGrafica.setTotalPuntasCalendarizado(totalPuntasCalendarizado.toString());
            detalleGrafica.setTotalRentaCalendarizado(totalRentaCalendarizado.toString());
            detalleGrafica.setTotalPuntasDetenido(totalPuntasDetenido.toString());
            detalleGrafica.setTotalRentaDetenido(totalRentaDetenido.toString());
            detalleGrafica.setTotalPuntasDevueltoVentas(totalPuntasDevueltoVentas.toString());
            detalleGrafica.setTotalRentaDevueltoVentas(totalRentaDevueltoVentas.toString());
            detalleGrafica.setTotalPuntasCancelado(totalPuntasCancelado.toString());
            detalleGrafica.setTotalRentaCancelado(totalRentaCancelado.toString());
            detalleGrafica.setTotalPuntasInstalado(totalPuntasInstalado.toString());
            detalleGrafica.setTotalRentaInstalado(totalRentaInstalado.toString());
            //--Grafica mes anterior
            detalleGrafica.setDetalleGrafica(arrElementosGrafica);
            detalleGrafica.setTotalRentaInstaladoMA(totalRentaInstaladoMA.toString());
            detalleGrafica.setTotalPuntasInstaladoMA(totalPuntasInstaladasMA.toString());
            detalleGrafica.setTotalRentaBackMA(totalRentaBackMA.toString());
            detalleGrafica.setTotalPuntasBackMA(totalPuntasBackMA.toString());
            detalleGrafica.setPorInstalarMA(PorInstalarMA.toString());
            detalleGrafica.setCalendarizadoMA(CalendarizadoMA.toString());
            detalleGrafica.setDetenidoMA(DetenidoMA.toString());
            detalleGrafica.setDevueltoVentasMA(DevueltoVentasMA.toString());
            detalleGrafica.setCanceladoMA(CanceladoMA.toString());
            
        }catch(Exception ex){
            LocalUtils.console(Constantes.C_TYPE_GEN_ERR, "Error : " + ex.toString(), "calculaGraficaGeneral");
        }
        
        return detalleGrafica;
    }
    
    private static ElementoGrafica loadElementosGrafica(String nombreX,Integer contadorOK,Integer contadorMD,Integer contadorMA,Integer contadorDV){
        
        ElementoGrafica elementoGrafica = new ElementoGrafica();
        
        try{
            
            elementoGrafica.setNombreX(nombreX);
            elementoGrafica.setContadorOK(contadorOK.toString());
            elementoGrafica.setColorOK(Variables.getCOLOR_OK());
            elementoGrafica.setContadorMD(contadorMD.toString());
            elementoGrafica.setColorMD(Variables.getCOLOR_M());
            elementoGrafica.setContadorMA(contadorMA.toString());
            elementoGrafica.setColorMA(Variables.getCOLOR_B());
            elementoGrafica.setContadorDV(contadorDV.toString());
            elementoGrafica.setColorDV(Variables.getCOLOR_SB());
            
        }catch(Exception ex){
            LocalUtils.console(Constantes.C_TYPE_GEN_ERR, "Error : " + ex.toString(), "loadElementosGrafica");
        }
        
        return elementoGrafica;
    }
}
