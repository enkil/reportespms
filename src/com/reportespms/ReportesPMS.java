package com.reportespms;

import com.reportespms.process.CatalogoStatus;
import com.reportespms.process.GraficaReporteGeneralPMs;
import com.reportespms.process.NoticiasHomePMs;
import com.reportespms.process.ReporteGeneralPMS;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

@Path("ReportesPMS")
public class ReportesPMS {
    public ReportesPMS() {
        super();
    }
    
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    @Path("ReporteGeneralExport")
    public Response ReporteGeneralPMS(String InputReporteGeneral){
        return new ReporteGeneralPMS().ReporteGeneralPMs(InputReporteGeneral);    
    }

    @POST
    @Consumes("application/json")
    @Produces("application/json")
    @Path("GraficaReporteGeneral")
    public Response GraficaReporteGeneralPMs(String InputGraficaReporteGeneralPMs){
        return new GraficaReporteGeneralPMs().graficaReporteGeneralPMs(InputGraficaReporteGeneralPMs);
    }

    @POST
    @Consumes("application/json")
    @Produces("application/json")
    @Path("NoticiasHomePMs")
    public Response noticiasHomePMs(String InputNoticiasHomePMs){
        return new NoticiasHomePMs().loadNewsPMs(InputNoticiasHomePMs);            
    }

    @POST
    @Consumes("application/json")
    @Produces("application/json")
    @Path("statusEIMs")
    public Response catalogosPMs(String inputStatus){
        return new CatalogoStatus().getStatusOTs(inputStatus);
    }
}
