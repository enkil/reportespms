package com.reportespms.inputs;

import com.reportespms.baseobjects.Login;

public class InputStatus {
    private Login login;
    private String idNegocio;
    
    public Login getLogin() {
            return login;
    }
    public void setLogin(Login login) {
            this.login = login;
    }
    public String getIdNegocio() {
            return idNegocio;
    }
    public void setIdNegocio(String idNegocio) {
            this.idNegocio = idNegocio;
    }
}
