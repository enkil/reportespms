package com.reportespms.inputs;

import com.reportespms.baseobjects.Login;
import com.reportespms.utils.LocalUtils;

public class InputGraficaReporteGeneralPMs {
    
    private Login Login;
    private String idPm;
    private String tipoFiltro;
    private String fecha;
    
    public InputGraficaReporteGeneralPMs() {
        super();
    }

    public void setLogin(Login Login) {
        this.Login = Login;
    }

    public Login getLogin() {
        return Login;
    }

    public void setIdPm(String idPm) {
        this.idPm = LocalUtils.sanitizaParametros(idPm);
    }

    public String getIdPm() {
        return idPm;
    }

    public void setTipoFiltro(String tipoFiltro) {
        this.tipoFiltro = LocalUtils.sanitizaParametros(tipoFiltro);
    }

    public String getTipoFiltro() {
        return tipoFiltro;
    }

    public void setFecha(String fecha) {
        this.fecha = LocalUtils.sanitizaParametros(fecha);
    }

    public String getFecha() {
        return fecha;
    }
}
