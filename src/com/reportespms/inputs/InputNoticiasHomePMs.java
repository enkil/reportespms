package com.reportespms.inputs;

import com.reportespms.baseobjects.Login;
import com.reportespms.utils.LocalUtils;

public class InputNoticiasHomePMs {
    
    private Login Login;
    private String idPm;
    
    public InputNoticiasHomePMs() {
        super();
    }

    public void setLogin(Login Login) {
        this.Login = Login;
    }

    public Login getLogin() {
        return Login;
    }

    public void setIdPm(String idPm) {
        this.idPm = LocalUtils.sanitizaParametros(idPm);
    }

    public String getIdPm() {
        return idPm;
    }
}
