package com.reportespms.hilos;

import com.connectionsfffm.objetos.OutputsSF;

import com.reportespms.baseobjects.Constantes;
import com.reportespms.baseobjects.Variables;
import com.reportespms.objects.Implementacion;
import com.reportespms.objects.Proyecto;

import com.reportespms.utils.LocalUtils;

import com.sforce.soap.partner.QueryResult;

import com.sforce.soap.partner.sobject.SObject;

import java.util.HashMap;
import java.util.Map;

public class HiloProyectos{
    
    
    public HiloProyectos() {
        super();
    }
    
    
    public Map<String,Implementacion> loadDataProyecto(String idPm){
    
        Map<String,Implementacion> mapProyect = new HashMap<String,Implementacion>();
        
        try{
            
            OutputsSF outputSf = Variables.getCONECTION_SF().consultaSalesforce(Constantes.QR_GET_PROYECTOS_PM.replaceAll("idPm", idPm));
            
            if(outputSf.getResult()){
                QueryResult reSsf = outputSf.getQueryResult();
                if(reSsf.getSize()>0){
                    for(SObject cl :  reSsf.getRecords()){
                        Implementacion pr = new Implementacion();
                        pr.setIdCsp((String) cl.getField("Id"));
                        pr.setFolioCsp((String) cl.getField("Name"));
                        
                        if(Constantes.JAR_UTILS.validateDataSF(cl, "Cotizacion__r")){
                            SObject cot = (SObject) cl.getField("Cotizacion__r");
                            if(Constantes.JAR_UTILS.validateDataSF(cot, "Oportunidad__r")){
                                SObject opor = (SObject) cot.getField("Oportunidad__r");
                                if(Constantes.JAR_UTILS.validateDataSF(opor, "Account")){
                                    SObject ac = (SObject) opor.getField("Account");
                                    pr.setNombreCliente((String) ac.getField("RazonSocial__c"));
                                }
                            }
                        }
                        
                        mapProyect.put((String) cl.getField("Id"), pr);
                    }
                }else{
                    LocalUtils.console(Constantes.C_TYPE_INFO, "Sin informacion", "loadDataProyecto");    
                }    
            }else{
                LocalUtils.console(Constantes.C_TYPE_ERR_SF, outputSf.getResultDescripcion(),"loadDataProyecto");
            }
        
        }catch(Exception ex){
            LocalUtils.console(Constantes.C_TYPE_GEN_ERR, "Error : " + ex.toString(), "loadDataProyecto");
        }
        
        return mapProyect;
            
    }
}
