package com.reportespms.hilos;

import com.connectionsfffm.objetos.OutputsSF;

import com.reportespms.baseobjects.Constantes;
import com.reportespms.baseobjects.Variables;
import com.reportespms.objects.Hoy;
import com.reportespms.objects.Implementacion;
import com.reportespms.utils.LocalUtils;

import com.sforce.soap.partner.sobject.SObject;

import com.sun.xml.internal.bind.v2.model.core.ID;

import java.util.ArrayList;

public class HiloCambiosStatusOS extends Thread{
    
    private String idPm;
    private ArrayList<Hoy> listNotificacionesImplementacion;
    private ArrayList<Hoy> listNotificacionesHoy;
    private Implementacion[] detalleCambiosImplementacion;
        
    public HiloCambiosStatusOS(String idPm) {
        this.idPm=idPm;
    }
    
    @Override
    public void run() {
        this.setDetalleCambiosImplementacion(this.getCambiosImplementacion(idPm));
    }
    
    public Implementacion[] getCambiosImplementacion(String idPm){
        
        Implementacion[] cambiosImplementacion = new Implementacion[0];
        
        try{
            
            System.out.println("Inicio cambios implementacion");
            
            OutputsSF outputSf = Variables.getCONECTION_SF().consultaSalesforce(Constantes.QR_GET_ESTATUS_OS_SF.replaceAll("idPm", idPm));
            
            if(outputSf.getResult()){
                if(outputSf.getQueryResult().getSize()>0){
                    listNotificacionesHoy = new ArrayList<Hoy>();
                    listNotificacionesImplementacion = new ArrayList<Hoy>();
                    cambiosImplementacion = new Implementacion[outputSf.getQueryResult().getSize()];
                    int cont = 0;
                    for(SObject cambioCsp : outputSf.getQueryResult().getRecords()){
                        Implementacion cambioImple = new Implementacion();
                        
                        cambioImple.setIdCsp((String) cambioCsp.getField("Id"));
                        cambioImple.setFolioCsp((String) cambioCsp.getField("Name"));
                            
                        if(Constantes.JAR_UTILS.validateDataSF(cambioCsp, "Cotizacion__r")){
                            SObject cotizacion = (SObject) cambioCsp.getField("Cotizacion__r");
                            
                            cambioImple.setIdCotizacion((String) cotizacion.getField("Id"));
                            cambioImple.setFolioCotizacion((String) cotizacion.getField("Name"));
                            cambioImple.setTop5mil((String) cotizacion.getField("Top5mil__c"));
                            cambioImple.setSegmentoGerencia((String) cotizacion.getField("Segmento_Gerencia__c"));
                            
                            if(Constantes.JAR_UTILS.validateDataSF(cotizacion, "Oportunidad__r")){
                                SObject oportunidad = (SObject) cotizacion.getField("Oportunidad__r");
                                if(Constantes.JAR_UTILS.validateDataSF(oportunidad, "Account")){
                                    SObject cuenta = (SObject) oportunidad.getField("Account");
                                    
                                    cambioImple.setNombreCliente((String) cuenta.getField("RazonSocial__c"));
                                    
                                    if(Constantes.JAR_UTILS.validateDataSF(cuenta, "ContactoPrincipal__r")){
                                        SObject contacto = (SObject) cuenta.getField("ContactoPrincipal__r");
                                        
                                        cambioImple.setNombreContactoPrincipal((String) contacto.getField("Name"));
                                        cambioImple.setTelefonoContactoPrincipal((String) contacto.getField("MobilePhone"));
                                    }
                                }
                            } 
                        }
                        
                          
                        
                        if(Constantes.JAR_UTILS.validateDataSF(cambioCsp, "OrdenServicio__r")){
                            SObject os = (SObject) cambioCsp.getField("OrdenServicio__r");
                            
                            cambioImple.setIdOs((String) os.getField("Id"));
                            cambioImple.setFolioOs((String) os.getField("Name"));
                            cambioImple.setEstatusOs((String) os.getField("Estatus__c"));
                            cambioImple.setIdOtFfm((String) os.getField("IdOtGIM__c"));
                            cambioImple.setComentariosOs(os.getField("ComentariosGIM__c") != null ? (String) os.getField("ComentariosGIM__c") : "Sin comentarios registrados en Salesforce");
                            cambioImple.setLabelInicio(os.getField("TSInicio__c") != null ? Constantes.L_INICIO : null);
                            cambioImple.setFechaInicio(LocalUtils.getCleanDateSF((String) os.getField("TSInicio__c")));
                            cambioImple.setLabelAgendado(os.getField("TSAgendado__c")!=null ? Constantes.L_AGENDADO : null);
                            cambioImple.setFechaAgendado(LocalUtils.getCleanDateSF((String) os.getField("TSAgendado__c")));
                            cambioImple.setLabelConfirmado(os.getField("TSConfirmado__c") != null ? Constantes.L_CONFIRMADO : null);
                            cambioImple.setFechaConfirmado(LocalUtils.getCleanDateSF((String) os.getField("TSConfirmado__c")));
                            cambioImple.setLabelCompletado(os.getField("TSCompletado__c") != null ? Constantes.L_COMPLETADO : null);
                            cambioImple.setFechaCompletado(LocalUtils.getCleanDateSF((String) os.getField("TSCompletado__c")));
                            cambioImple.setLabelRescate(os.getField("TSRescate__c") != null ? Constantes.L_RESCATE : null);
                            cambioImple.setFechaRescate(LocalUtils.getCleanDateSF((String) os.getField("TSRescate__c")));
                            cambioImple.setLabelCancelado(os.getField("TSCancelado__c") != null ? Constantes.L_CANCELADO : null);
                            cambioImple.setFechaCancelado(LocalUtils.getCleanDateSF((String) os.getField("TSCancelado__c")));
                            cambioImple.setLabelEnProceso(os.getField("TSEnProceso__c") != null ? Constantes.L_ENPROCESO : null);
                            cambioImple.setFechaEnProceso(LocalUtils.getCleanDateSF((String) os.getField("TSEnProceso__c")));
                            cambioImple.setLabelCalendarizado(os.getField("TSCalendarizado__c") != null ? Constantes.L_CALENDARIZADO : null);
                            cambioImple.setFechaCalendarizado(LocalUtils.getCleanDateSF((String) os.getField("TSCalendarizado__c")));
                            cambioImple.setLabelDetenida(os.getField("TSDetenida__c") != null ? Constantes.L_DETENIDA : null);
                            cambioImple.setFechaDetenida(LocalUtils.getCleanDateSF((String) os.getField("TSDetenida__c")));
                            cambioImple.setLabelSsuspendido(os.getField("TSSsuspendido__c") != null ? Constantes.L_SUSPENDIDO : null);
                            cambioImple.setFechaSsuspendido(LocalUtils.getCleanDateSF((String) os.getField("TSSsuspendido__c")));
                            cambioImple.setLabelGestoria(os.getField("TSGestoria__c") != null ? Constantes.L_GESTORIA : null);
                            cambioImple.setFechaGestoria(LocalUtils.getCleanDateSF((String) os.getField("TSGestoria__c")));
                        }
                        
                        Hoy nuevaHoy = new Hoy();
                        nuevaHoy.setIdBrinco(cambioImple.getIdCsp());
                        nuevaHoy.setTipoNotificacion(Constantes.C_TYPE_UPDATE_STATUS_IMPLEMENTACION);
                        nuevaHoy.setNivelUno(cambioImple.getNombreCliente());
                        nuevaHoy.setNivelDos(Constantes.C_DESCRIPCION_CAMBIO_ACTIVIDAD);
                        nuevaHoy.setNivelTres(cambioImple.getFolioCsp());
                        nuevaHoy.setNivelCuatro("Estatus OS : " + cambioImple.getEstatusOs());
                        nuevaHoy.setColorNotificacionTxt(Variables.getTXTCOLOR_IMP());
                        nuevaHoy.setColorNotificacionBg(Variables.getBGCOLOR_IMP());
                        nuevaHoy.setDescNotificacion(Variables.getDESC_IMO());
                        
                        if(LocalUtils.getEventoDetonadoHoy(cambioImple.getFechaInicio()) || LocalUtils.getEventoDetonadoHoy(cambioImple.getFechaAgendado()) || LocalUtils.getEventoDetonadoHoy(cambioImple.getFechaConfirmado()) || LocalUtils.getEventoDetonadoHoy(cambioImple.getFechaCompletado()) ||
                        LocalUtils.getEventoDetonadoHoy(cambioImple.getFechaRescate()) || LocalUtils.getEventoDetonadoHoy(cambioImple.getFechaCancelado()) || LocalUtils.getEventoDetonadoHoy(cambioImple.getFechaEnProceso()) || LocalUtils.getEventoDetonadoHoy(cambioImple.getFechaCalendarizado())
                        || LocalUtils.getEventoDetonadoHoy(cambioImple.getFechaDetenida()) || LocalUtils.getEventoDetonadoHoy(cambioImple.getFechaSsuspendido()) || LocalUtils.getEventoDetonadoHoy(cambioImple.getFechaGestoria())){ //--- Ver como se va a trabajar el tema de las fechas
                            listNotificacionesHoy.add(nuevaHoy);
                        }
                        
                        listNotificacionesImplementacion.add(nuevaHoy);
                        
                        cambiosImplementacion[cont++] = cambioImple;
                    }   
                    setListNotificacionesHoy(listNotificacionesHoy); 
                }else{
                    LocalUtils.console(Constantes.C_TYPE_INFO, "Sin cambios : " + outputSf.getResultDescripcion(),  "getCambiosImplementacion");    
                }
            }else{
                LocalUtils.console(Constantes.C_TYPE_ERR_SF, "Error al cargar cambios en csp : " + outputSf.getResultDescripcion(),  "getCambiosImplementacion");
            }
            
        }catch(Exception ex){
            LocalUtils.console(Constantes.C_TYPE_GEN_ERR, "Error en cambio implementacion : " + ex.toString(),  "getCambiosImplementacion");
        }   
        
        System.out.println("Fin cambios implementacion");
        
        return cambiosImplementacion;
    }

    public void setDetalleCambiosImplementacion(Implementacion[] detalleCambiosImplementacion) {
        this.detalleCambiosImplementacion = detalleCambiosImplementacion;
    }

    public Implementacion[] getDetalleCambiosImplementacion() {
        return detalleCambiosImplementacion;
    }

    public void setListNotificacionesHoy(ArrayList<Hoy> listNotificacionesHoy) {
        this.listNotificacionesHoy = listNotificacionesHoy;
    }

    public ArrayList<Hoy> getListNotificacionesHoy() {
        return listNotificacionesHoy;
    }

    public void setListNotificacionesImplementacion(ArrayList<Hoy> listNotificacionesImplementacion) {
        this.listNotificacionesImplementacion = listNotificacionesImplementacion;
    }

    public ArrayList<Hoy> getListNotificacionesImplementacion() {
        return listNotificacionesImplementacion;
    }
}
