package com.reportespms.hilos;

import com.connectionsfffm.objetos.OutputsFFM;

import com.reportespms.baseobjects.Constantes;
import com.reportespms.baseobjects.Variables;
import com.reportespms.objects.Actividad;
import com.reportespms.objects.Hoy;
import com.reportespms.objects.Implementacion;
import com.reportespms.utils.LocalUtils;

import com.sforce.soap.partner.QueryResult;

import java.util.ArrayList;
import java.util.Map;

public class HiloActividadesMes extends Thread{
    
    private String idPm;
    private Map<String,Implementacion> mapDataProyect;
    private ArrayList<Hoy> listNotificacionesActividades;
    private ArrayList<Hoy> listNotificacionesHoy;
    private Actividad[] detalleActividadesPM;
    
    public HiloActividadesMes(String idPm,Map<String,Implementacion> mapDataProyect) {
        this.idPm=idPm;
        this.mapDataProyect = mapDataProyect;
    }
    
    @Override
    public void run() {
        this.setDetalleActividadesPM(this.getDetalleActividadesMes(idPm,mapDataProyect));
    }
    
    public Actividad[] getDetalleActividadesMes(String idPm,Map<String,Implementacion> mapDataProyect){
        
        Actividad[] detalleActividad = new Actividad[0];
        
        try{
        
           OutputsFFM outputFfm =  Constantes.CONECTION_FFM.consultaBD(Constantes.QR_GET_ACTIVIDADES_MES.replaceAll("idPm", idPm));
            
            if(outputFfm.getResult()){
                String actividadesPlaneadas[][] = outputFfm.getArr_records();
                if(actividadesPlaneadas.length>0){
                    listNotificacionesHoy = new ArrayList<Hoy>();
                    listNotificacionesActividades = new ArrayList<Hoy>();
                    detalleActividad = new Actividad[actividadesPlaneadas.length];
                    for(int fila = 0; fila <actividadesPlaneadas.length; fila++){
                        Actividad activ = new Actividad();
                        String nombreCliente = "";
                        String folioCsp = "";
                        String tipoNot = "";
                        String descNot = "";
                        
                        activ.setIdBridgeSf(actividadesPlaneadas[fila][0]);
                        activ.setIdActividad(actividadesPlaneadas[fila][1]);
                        activ.setDescripcionActividad(actividadesPlaneadas[fila][2]);
                        activ.setTieneOt(actividadesPlaneadas[fila][3]);
                        activ.setFechaInicioPlabeada(actividadesPlaneadas[fila][4]);
                        activ.setFechaFinPlaneada(actividadesPlaneadas[fila][5]);
                        activ.setFechaInicioReal(actividadesPlaneadas[fila][6]);
                        activ.setFechaFinReal(actividadesPlaneadas[fila][7]);
                        activ.setAvance(actividadesPlaneadas[fila][8]);
                        activ.setIdDependencia(actividadesPlaneadas[fila][9]);
                        activ.setNombreResponsable(actividadesPlaneadas[fila][10]);
                        activ.setTelefonoResponsable(actividadesPlaneadas[fila][11]);
                        activ.setEstatusPlaneacion(actividadesPlaneadas[fila][12].equals("0") ? "Planeacion abierta" : "Planeacion cerrada");
                        activ.setFechaActual(actividadesPlaneadas[fila][13]);
                        
                        Hoy nuevaHoy = null;
                        if(actividadesPlaneadas[fila][14].equals("true") && LocalUtils.getEventoDetonadoHoy(activ.getFechaInicioReal())){
                            Implementacion imp = mapDataProyect.get(activ.getIdBridgeSf());
                            if(imp!=null){
                                nuevaHoy = new Hoy();
                                folioCsp = imp.getFolioCsp();
                                nombreCliente = imp.getNombreCliente();
                                tipoNot = Constantes.C_TYPE_INTOFIN_ACTIVIDAD;
                                descNot = Constantes.C_DESCRIPCION_INTOFIN_ACTIVIDAD;
                            }
                        }else if(LocalUtils.getEventoDetonadoHoy(activ.getFechaInicioReal())){ 
                            Implementacion imp = mapDataProyect.get(activ.getIdBridgeSf());
                            if(imp!=null){
                                nuevaHoy = new Hoy();
                                folioCsp = imp.getFolioCsp();
                                nombreCliente = imp.getNombreCliente();
                                tipoNot = Constantes.C_TYPE_INICIO_ACTIVIDAD;
                                descNot = Constantes.C_DESCRIPCION_INICIO_ACTIVIDAD;
                            }
                            
                        }else if(LocalUtils.getEventoDetonadoHoy(activ.getFechaFinReal())){
                            Implementacion imp = mapDataProyect.get(activ.getIdBridgeSf());
                            if(imp!=null){
                                nuevaHoy = new Hoy();
                                folioCsp = imp.getFolioCsp();
                                nombreCliente = imp.getNombreCliente();
                                tipoNot = Constantes.C_TYPE_FIN_ACTIVIDAD;
                                descNot = Constantes.C_DESCRIPCION_FIN_ACTIVIDAD;
                            }
                            
                        }else{
                            tipoNot = Constantes.C_TYPE_FIN_ACTIVIDAD;
                            descNot = Constantes.C_DESCRIPCION_FIN_ACTIVIDAD; 
                        }
                        
                        
                        Hoy hoy = new Hoy();
                        Implementacion imppp = mapDataProyect.get(activ.getIdBridgeSf());
                        if(imppp!=null){
                            folioCsp = imppp.getFolioCsp();
                            nombreCliente = imppp.getNombreCliente();
                            hoy.setIdBrinco(activ.getIdBridgeSf());
                            hoy.setTipoNotificacion(tipoNot);
                            hoy.setNivelUno(nombreCliente);
                            hoy.setNivelDos(descNot);
                            hoy.setNivelTres(folioCsp);
                            hoy.setNivelCuatro(activ.getDescripcionActividad());
                            hoy.setColorNotificacionTxt(Variables.getTXTCOLOR_ACT());
                            hoy.setColorNotificacionBg(Variables.getBGCOLOR_ACT());
                            hoy.setDescNotificacion(Variables.getDESC_ACT());    
                            listNotificacionesActividades.add(hoy);
                        }
                        
                        if(nuevaHoy!=null){
                            nuevaHoy.setIdBrinco(activ.getIdBridgeSf());
                            nuevaHoy.setTipoNotificacion(tipoNot);
                            nuevaHoy.setNivelUno(nombreCliente);
                            nuevaHoy.setNivelDos(descNot);
                            nuevaHoy.setNivelTres(folioCsp);
                            nuevaHoy.setNivelCuatro(activ.getDescripcionActividad());
                            nuevaHoy.setColorNotificacionTxt(Variables.getTXTCOLOR_ACT());
                            nuevaHoy.setColorNotificacionBg(Variables.getBGCOLOR_ACT());
                            nuevaHoy.setDescNotificacion(Variables.getDESC_ACT());    
                            listNotificacionesHoy.add(nuevaHoy);
                        }
                        
                        activ.setTipoNotificacion(tipoNot);
                        activ.setNivelUno(nombreCliente);
                        activ.setNivelDos(descNot);
                        activ.setNivelTres(folioCsp);
                        activ.setColorNotificacionTxt(Variables.getTXTCOLOR_ACT());
                        activ.setColorNotificacionBg(Variables.getBGCOLOR_ACT());
                        activ.setDescNotificacion(Variables.getDESC_ACT()); 
                        detalleActividad[fila] = activ;
                    }   
                    setListNotificacionesHoy(listNotificacionesHoy); 
                }else{
                    LocalUtils.console(Constantes.C_TYPE_ERR_FFM, "Sin actividades programadas", "getDetalleActividadesMes");        
                }        
            }else{
                LocalUtils.console(Constantes.C_TYPE_ERR_SF, outputFfm.getResultDescripcion(), "getDetalleActividadesMes");    
            } 
            
        }catch(Exception ex){
            LocalUtils.console(Constantes.C_TYPE_GEN_ERR, "Error en actividades mes : " + ex.toString(),  "getDetalleActividadesMes");
        }
        
        return detalleActividad;
            
    }

    public void setDetalleActividadesPM(Actividad[] detalleActividadesPM) {
        this.detalleActividadesPM = detalleActividadesPM;
    }

    public Actividad[] getDetalleActividadesPM() {
        return detalleActividadesPM;
    }

    public void setListNotificacionesHoy(ArrayList<Hoy> listNotificacionesHoy) {
        this.listNotificacionesHoy = listNotificacionesHoy;
    }

    public ArrayList<Hoy> getListNotificacionesHoy() {
        return listNotificacionesHoy;
    }

    public void setListNotificacionesActividades(ArrayList<Hoy> listNotificacionesActividades) {
        this.listNotificacionesActividades = listNotificacionesActividades;
    }

    public ArrayList<Hoy> getListNotificacionesActividades() {
        return listNotificacionesActividades;
    }
}
