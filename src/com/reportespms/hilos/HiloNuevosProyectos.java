package com.reportespms.hilos;

import com.connectionsfffm.objetos.OutputsFFM;

import com.connectionsfffm.objetos.OutputsSF;

import com.reportespms.baseobjects.Constantes;
import com.reportespms.baseobjects.Variables;
import com.reportespms.objects.Hoy;
import com.reportespms.objects.Proyecto;
import com.reportespms.utils.LocalUtils;

import com.sforce.soap.partner.QueryResult;
import com.sforce.soap.partner.sobject.SObject;

import com.sun.xml.internal.bind.v2.model.core.ID;

import java.util.ArrayList;

public class HiloNuevosProyectos extends Thread{
    
    private Proyecto arrNuevosProyectos[];
    private ArrayList<Hoy> listNotificacionesProyectos;
    private ArrayList<Hoy> listNotificacionesHoy;
    private String idPm;
    
    public HiloNuevosProyectos(String idPm) {
        this.idPm = idPm;
    }
    
    @Override
    public void run() {
        this.setArrNuevosProyectos(this.getNuevosProyectos(idPm));
    }
    
    private Proyecto[] getNuevosProyectos(String idPm){
        
        Proyecto arrProyectosNuevos[] = new Proyecto[0];
        
        try{
            
            System.out.println("Inicio nuevos proyectos");
            
            OutputsSF outSf = Variables.getCONECTION_SF().consultaSalesforce(Constantes.QR_GET_NUEVOS_PROYECTOS_PM.replaceAll("idPm", idPm));
            
            if(outSf.getResult()){
                if(outSf.getQueryResult().getSize()>0){
                    arrProyectosNuevos = new Proyecto[outSf.getQueryResult().getSize()];
                    listNotificacionesHoy = new ArrayList<Hoy>();
                    listNotificacionesProyectos = new ArrayList<Hoy>();
                    int cont = 0;
                    for(SObject newProyect : outSf.getQueryResult().getRecords()){
                        Proyecto nuevoProyecto = new Proyecto();
                        nuevoProyecto.setIdCotizacion((String) newProyect.getField("Id"));
                        nuevoProyecto.setFolioCotizacion((String) newProyect.getField("Name"));
                        nuevoProyecto.setPuntasTotales((String) newProyect.getField("PuntasTotales__c"));
                        nuevoProyecto.setTop5mil((String) newProyect.getField("Top5mil__c"));
                        nuevoProyecto.setSegmentoGerencia((String) newProyect.getField("Segmento_Gerencia__c"));
                        nuevoProyecto.setFechaAsignacion(LocalUtils.getCleanDateSF((String) newProyect.getField("TS_AsignacionPM__c")));
                        
                            if(Constantes.JAR_UTILS.validateDataSF(newProyect, "Oportunidad__r")){
                                SObject oportunidad = (SObject) newProyect.getField("Oportunidad__r");
                                nuevoProyecto.setFechaCerradaGanada(LocalUtils.getCleanDateSF((String) oportunidad.getField("TsGanada__c"))); 
                                nuevoProyecto.setSegmento((String) oportunidad.getField("Segmento__c"));
                                nuevoProyecto.setPlaza((String) oportunidad.getField("Plaza__c"));
                                
                                if(Constantes.JAR_UTILS.validateDataSF(oportunidad, "Account")){
                                    SObject cuenta = (SObject) oportunidad.getField("Account");
                                    nuevoProyecto.setNombreCliente((String) cuenta.getField("RazonSocial__c")); 
                                    if(Constantes.JAR_UTILS.validateDataSF(cuenta, "ContactoPrincipal__r")){
                                        SObject contacto = (SObject) cuenta.getField("ContactoPrincipal__r");
                                        nuevoProyecto.setNombreContactoPrincipal((String) contacto.getField("Name")); 
                                        nuevoProyecto.setTelefonoContactoPrincipal((String) contacto.getField("MobilePhone"));
                                    }
                                }
                                
                                if(Constantes.JAR_UTILS.validateDataSF(oportunidad, "Owner")){
                                    SObject vendedor = (SObject) oportunidad.getField("Owner");
                                    nuevoProyecto.setNombreVendedor(vendedor.getField("Name") != null ? (String) vendedor.getField("Name") : "Sin informacion en Salesforce");
                                    nuevoProyecto.setTelefonoVendedor(vendedor.getField("MobilePhone") != null ? (String) vendedor.getField("MobilePhone") : "NA"); 
                                    nuevoProyecto.setEmailVendedor(vendedor.getField("Email") !=null ? (String) vendedor.getField("Email") : "NA"); 
                                }else{
                                    nuevoProyecto.setNombreVendedor("Sin informacion en Salesforce");
                                    nuevoProyecto.setTelefonoVendedor("NA"); 
                                    nuevoProyecto.setEmailVendedor("NA");
                                }
                                
                            }
                        
                        Hoy nuevaHoy = new Hoy();
                        nuevaHoy.setIdBrinco(nuevoProyecto.getIdCotizacion());
                        nuevaHoy.setTipoNotificacion(Constantes.C_TYPE_PROYECTO_NUEVO);
                        nuevaHoy.setNivelUno(nuevoProyecto.getNombreCliente());
                        nuevaHoy.setNivelDos(Constantes.C_DESCRIPCION_PROYECTO_NUEVO);
                        nuevaHoy.setNivelTres(nuevoProyecto.getFolioCotizacion());
                        nuevaHoy.setNivelCuatro("Vendedor : "+nuevoProyecto.getNombreVendedor());
                        nuevaHoy.setColorNotificacionTxt(Variables.getTXTCOLOR_PRO());
                        nuevaHoy.setColorNotificacionBg(Variables.getBGCOLOR_PRO());
                        nuevaHoy.setDescNotificacion(Variables.getDESC_PRO());
                        
                        if(LocalUtils.getEventoDetonadoHoy(nuevoProyecto.getFechaAsignacion())){
                            listNotificacionesHoy.add(nuevaHoy);
                        }
                        
                        listNotificacionesProyectos.add(nuevaHoy);
                        setListNotificacionesProyectos(listNotificacionesProyectos);
                        arrProyectosNuevos[cont++] = nuevoProyecto;
                    }
                    setListNotificacionesHoy(listNotificacionesHoy);
                }else{
                    LocalUtils.console(Constantes.C_TYPE_INFO, "Sin resultados", "getNuevosProyectos");    
                }
            }else{
                LocalUtils.console(Constantes.C_TYPE_ERR_SF, "Error al consultar en SF : " + outSf.getResultDescripcion(), "getNuevosProyectos");    
            }
        
        }catch(Exception ex){
            LocalUtils.console(Constantes.C_TYPE_GEN_ERR, "Error al cargar nuevos proyectos : " + ex.toString(), "getNuevosProyectos");
        }
        
        System.out.println("Fin nuevos proyectos");
        
        return arrProyectosNuevos;
        
    }

    public void setArrNuevosProyectos(Proyecto[] arrNuevosProyectos) {
        this.arrNuevosProyectos = arrNuevosProyectos;
    }

    public Proyecto[] getArrNuevosProyectos() {
        return arrNuevosProyectos;
    }

    public void setListNotificacionesHoy(ArrayList<Hoy> listNotificacionesHoy) {
        this.listNotificacionesHoy = listNotificacionesHoy;
    }

    public ArrayList<Hoy> getListNotificacionesHoy() {
        return listNotificacionesHoy;
    }

    public void setListNotificacionesProyectos(ArrayList<Hoy> listNotificacionesProyectos) {
        this.listNotificacionesProyectos = listNotificacionesProyectos;
    }

    public ArrayList<Hoy> getListNotificacionesProyectos() {
        return listNotificacionesProyectos;
    }
}
