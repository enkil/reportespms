package com.reportespms.outputs;

import com.reportespms.objects.Actividad;
import com.reportespms.objects.Hoy;
import com.reportespms.objects.Implementacion;
import com.reportespms.objects.Proyecto;

public class OutputNoticiasHomePMs {
    
    private String result;
    private String resultDescripcion;
    private String version;
    private Hoy notificacionesHoy[];
    private Hoy notificacioneProyectos[];
    private Hoy notificacionesImplementacion[];
    private Hoy notificacionesActividades[];    
    private Proyecto detalleProyectosMes[];
    private Actividad detalleActividadesMes[];
    private Implementacion detalleImplementacionMes[];
    
    public OutputNoticiasHomePMs() {
        super();
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getResult() {
        return result;
    }

    public void setResultDescripcion(String resultDescripcion) {
        this.resultDescripcion = resultDescripcion;
    }

    public String getResultDescripcion() {
        return resultDescripcion;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getVersion() {
        return version;
    }

    public void setNotificacionesHoy(Hoy[] notificacionesHoy) {
        this.notificacionesHoy = notificacionesHoy;
    }

    public Hoy[] getNotificacionesHoy() {
        return notificacionesHoy;
    }

    public void setNotificacioneProyectos(Hoy[] notificacioneProyectos) {
        this.notificacioneProyectos = notificacioneProyectos;
    }

    public Hoy[] getNotificacioneProyectos() {
        return notificacioneProyectos;
    }

    public void setNotificacionesImplementacion(Hoy[] notificacionesImplementacion) {
        this.notificacionesImplementacion = notificacionesImplementacion;
    }

    public Hoy[] getNotificacionesImplementacion() {
        return notificacionesImplementacion;
    }

    public void setNotificacionesActividades(Hoy[] notificacionesActividades) {
        this.notificacionesActividades = notificacionesActividades;
    }

    public Hoy[] getNotificacionesActividades() {
        return notificacionesActividades;
    }

    public void setDetalleProyectosMes(Proyecto[] detalleProyectosMes) {
        this.detalleProyectosMes = detalleProyectosMes;
    }

    public Proyecto[] getDetalleProyectosMes() {
        return detalleProyectosMes;
    }

    public void setDetalleActividadesMes(Actividad[] detalleActividadesMes) {
        this.detalleActividadesMes = detalleActividadesMes;
    }

    public Actividad[] getDetalleActividadesMes() {
        return detalleActividadesMes;
    }

    public void setDetalleImplementacionMes(Implementacion[] detalleImplementacionMes) {
        this.detalleImplementacionMes = detalleImplementacionMes;
    }

    public Implementacion[] getDetalleImplementacionMes() {
        return detalleImplementacionMes;
    }


}
