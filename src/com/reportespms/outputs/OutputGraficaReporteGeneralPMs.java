package com.reportespms.outputs;

import com.reportespms.objects.GraficaPM;
import com.reportespms.objects.ReporteGeneral;

public class OutputGraficaReporteGeneralPMs {
    
    private String result;
    private String resultDescripcion;
    private String version;
    private GraficaPM detalleGraficas;
    private ReporteGeneral detalleTabla[];
        
    public OutputGraficaReporteGeneralPMs() {
        super();
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getResult() {
        return result;
    }

    public void setResultDescripcion(String resultDescripcion) {
        this.resultDescripcion = resultDescripcion;
    }

    public String getResultDescripcion() {
        return resultDescripcion;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getVersion() {
        return version;
    }

    public void setDetalleGraficas(GraficaPM detalleGraficas) {
        this.detalleGraficas = detalleGraficas;
    }

    public GraficaPM getDetalleGraficas() {
        return detalleGraficas;
    }

    public void setDetalleTabla(ReporteGeneral[] detalleTabla) {
        this.detalleTabla = detalleTabla;
    }

    public ReporteGeneral[] getDetalleTabla() {
        return detalleTabla;
    }
}
