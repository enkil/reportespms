package com.reportespms.outputs;

import com.reportespms.objects.ReporteGeneral;

public class OutputReporteGeneral {
    private String result;
    private String resultDescripcion;
    private String version;
    private ReporteGeneral reporteGeneral [];
    
    public OutputReporteGeneral() {
        super();
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getResult() {
        return result;
    }

    public void setResultDescripcion(String resultDescripcion) {
        this.resultDescripcion = resultDescripcion;
    }

    public String getResultDescripcion() {
        return resultDescripcion;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getVersion() {
        return version;
    }

    public void setReporteGeneral(ReporteGeneral[] reporteGeneral) {
        this.reporteGeneral = reporteGeneral;
    }

    public ReporteGeneral[] getReporteGeneral() {
        return reporteGeneral;
    }
}
