package com.reportespms.outputs;

import com.reportespms.objects.Catalogo;

import java.util.List;

public class OutputStatus {
    private String result;
    private String resultDescripcion;
    private Catalogo catalogos[];
    
    public String getResult() {
            return result;
    }
    public void setResult(String result) {
            this.result = result;
    }
    public String getResultDescripcion() {
            return resultDescripcion;
    }
    public void setResultDescripcion(String resultDescripcion) {
            this.resultDescripcion = resultDescripcion;
    }
    public void setCatalogos(Catalogo[] catalogos) {
        this.catalogos = catalogos;
    }

    public Catalogo[] getCatalogos() {
        return catalogos;
    }
}
