package com.reportespms.utils;

import com.connectionsfffm.objetos.OutputsFFM;
import com.connectionsfffm.utils.CrudFFM;

import com.reportespms.baseobjects.Constantes;

import java.sql.Connection;

public class ConeccionFFM {
    private CrudFFM crudFfm;
    
    public ConeccionFFM() {
        super();
        crudFfm = new CrudFFM();
    }
    
    public OutputsFFM consultaBD(String consulta) {
        
        OutputsFFM out_crud = new OutputsFFM();
        Connection conection = Constantes.CONECTION_DATA_BASE.creaConexion(Constantes.TIPO_CONEXION);
        
        try{
            out_crud = crudFfm.consultaBD(conection, consulta);
        }catch(Exception ex){
            System.err.println("Error al consultar ffm : " + ex.toString());
        }
    
        return out_crud;
    }
    
    public OutputsFFM actualizaDB(String consulta) {
        
        OutputsFFM out_crud = new OutputsFFM();
        Connection conection = Constantes.CONECTION_DATA_BASE.creaConexion(Constantes.TIPO_CONEXION);
        
        try{
            out_crud = crudFfm.actualizaDB(conection, consulta);
        }catch(Exception ex){
            System.err.println("Error al consultar ffm : " + ex.toString());
        }            
        return out_crud;
    }
    
    public OutputsFFM eliminaDB(String consulta) {
        
        OutputsFFM out_crud = new OutputsFFM();
        Connection conection = Constantes.CONECTION_DATA_BASE.creaConexion(Constantes.TIPO_CONEXION);
        
        try{
            out_crud = crudFfm.eliminaDB(conection, consulta);
        }catch(Exception ex){
            System.err.println("Error al consultar ffm : " + ex.toString());
        }
    
        return out_crud;
    }
    
    public OutputsFFM insertaDB(String consulta) {
        
        OutputsFFM out_crud = new OutputsFFM();
        Connection conection = Constantes.CONECTION_DATA_BASE.creaConexion(Constantes.TIPO_CONEXION);
        
        try{
            out_crud = crudFfm.insertaDB(conection, consulta);
        }catch(Exception ex){
            System.err.println("Error al consultar ffm : " + ex.toString());
        }
        
        return out_crud;
    }
    
    public OutputsFFM configuracionGeneral(String consulta) {
        
        OutputsFFM out_crud = new OutputsFFM();
        Connection conection = Constantes.CONECTION_DATA_BASE.creaConexion(Constantes.TIPO_CONEXION);
        
        try{
            out_crud = crudFfm.configuracionGeneral(conection, consulta);
        }catch(Exception ex){
            System.err.println("Error al consultar ffm : " + ex.toString());
        }
        
        return out_crud;
    }
}