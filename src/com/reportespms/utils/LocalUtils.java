package com.reportespms.utils;

import com.reportespms.baseobjects.Constantes;

import com.reportespms.baseobjects.Variables;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;

import org.owasp.html.PolicyFactory;
import static org.owasp.html.Sanitizers.BLOCKS;
import static org.owasp.html.Sanitizers.FORMATTING;
import static org.owasp.html.Sanitizers.IMAGES;
import static org.owasp.html.Sanitizers.LINKS;
import static org.owasp.html.Sanitizers.STYLES;
import static org.owasp.html.Sanitizers.TABLES;

public class LocalUtils {
    
    private static PolicyFactory sanitiser = BLOCKS.and(FORMATTING).and(IMAGES).and(LINKS).and(STYLES).and(TABLES);
    
    
    public static String sanitizaParametros(String parametros){
        return sanitiser.sanitize(parametros);    
    }
    
        public static void loadConfigIN_DB(){
            
            try{
                console(Constantes.C_TYPE_INFO,"Cargando Configuracion DB", "LocalUtils");
                Properties prop_in_db = Constantes.CONECTION_FFM.configuracionGeneral(Constantes.CS_CONFIG_IN_DB).getPropertiesInDB();
                Variables.setSF_US(prop_in_db.getProperty(Constantes.KEY_US_SF));
                Variables.setSF_PA(prop_in_db.getProperty(Constantes.KEY_PWS_SF));
                Variables.setSF_END_POINT(prop_in_db.getProperty(Constantes.KEY_ENDPOINT_SF));
                Variables.setCONECTION_SF(new ConeccionSF());
                Variables.setDIAS_PLAN(prop_in_db.getProperty(Constantes.KEY_DIAS_PLAN));
                Variables.setDIAS_SOLUCION(prop_in_db.getProperty(Constantes.KEY_DIAS_SOLUCION));
                Variables.setCOLOR_OK(prop_in_db.getProperty(Constantes.KEY_COLOR_OK));
                Variables.setCOLOR_M(prop_in_db.getProperty(Constantes.KEY_COLOR_M));
                Variables.setCOLOR_B(prop_in_db.getProperty(Constantes.KEY_COLOR_B));
                Variables.setCOLOR_SB(prop_in_db.getProperty(Constantes.KEY_COLOR_SB));
                Variables.setBGCOLOR_IMP(prop_in_db.getProperty(Constantes.KEY_BGCOLOR_IMP));
                Variables.setTXTCOLOR_IMP(prop_in_db.getProperty(Constantes.KEY_TXTCOLOR_IMP ));
                Variables.setDESC_ACT(prop_in_db.getProperty(Constantes.KEY_DESC_ACT ));
                Variables.setBGCOLOR_ACT(prop_in_db.getProperty(Constantes.KEY_BGCOLOR_ACT ));
                Variables.setTXTCOLOR_ACT(prop_in_db.getProperty(Constantes.KEY_TXTCOLOR_ACT ));
                Variables.setDESC_PRO(prop_in_db.getProperty(Constantes.KEY_DESC_PRO ));
                Variables.setBGCOLOR_PRO(prop_in_db.getProperty(Constantes.KEY_BGCOLOR_PRO ));
                Variables.setTXTCOLOR_PRO(prop_in_db.getProperty(Constantes.KEY_TXTCOLOR_PRO ));
                Variables.setDESC_IMO(prop_in_db.getProperty(Constantes.KEY_DESC_IMO));
                Variables.setQR_STATUS_ENL(prop_in_db.getProperty(Constantes.KEY_STATUS_ENL));
                Variables.setQR_STATUS_REC(prop_in_db.getProperty(Constantes.KEY_STATUS_REC));
                
                console(Constantes.C_TYPE_SUSSES,"Confuguracuin en DB cargada", "LocalUtils");
            }catch(Exception ex){
                console(Constantes.C_TYPE_GEN_ERR,"Error en loadConfigIN_DB " + ex.toString(), "LocalUtils");
            }    
        }

        public static void console(String typeMessage, String mensaje,String modul){
            
            try{
                String output = "";
                SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.US);
                Date fechaFinal=new Date();
                
                switch(typeMessage){
                    case Constantes.C_TYPE_INFO:
                        output = Constantes.C_MESSAGE_INFO;
                    break;
                    case Constantes.C_TYPE_SUSSES:
                        output = Constantes.C_MESSAGE_SUSSES;
                    break;
                    case Constantes.C_TYPE_ERR_FFM:
                        output = Constantes.C_MESSAGE_ERR_FFM;
                    break;
                    case Constantes.C_TYPE_ERR_SF:
                        output = Constantes.C_MESSAGE_ERR_SF;
                    break;
                    case Constantes.C_TYPE_GEN_ERR:
                        output = Constantes.C_MESSAGE_GENERAL_ERR;
                    break;
                }
                
                output = output + format.format(fechaFinal) + " - " + modul + " : " + mensaje;
                
                System.out.println(output);
            }catch(Exception ex){
                System.out.println(Constantes.C_MESSAGE_GENERAL_ERR + ex.toString());
            }
                
        }
        
        public Properties getConfiguracion() {
        Properties prop = null;
        try{
            prop = new Properties();
            prop.load(getClass().getResourceAsStream(Constantes.PATH_PROPERTIES));
        }catch(Exception ex){
            System.err.println("Error al obtener propiedades = " + ex);
        }
        
        return prop;
    }
            
    public static String calculaNumeroSemana(String fecha){
        
        String num_semana = "";
        
        try{
            if(fecha!= null){
                if(!fecha.equals("NA")){
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    Date date = sdf.parse(fecha);
                    Calendar fecha_p = Calendar.getInstance();
                    fecha_p.setFirstDayOfWeek( Calendar.MONDAY);
                    fecha_p.setMinimalDaysInFirstWeek( 4 );
                    fecha_p.setTime(date);
                    
                    num_semana = String.valueOf(fecha_p.get(Calendar.WEEK_OF_YEAR));    
                }else{
                    num_semana= "NA";    
                }
            }else{
                num_semana= "NA";    
            }
            
        }catch(Exception ex){
            System.err.println("Error al calcular num semana : " + ex.toString());    
        }
            
        return num_semana;    
    }
    
    public static String tiempoGanadaToActivacion(String ts_fecha,String fechaActivacion){
    
        String tiempo_ganada = "";
        
        try{
            
            if(ts_fecha!= null && fechaActivacion!= null){
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                
                Date fechaFinal = format.parse(fechaActivacion.replaceAll("T", " ").replaceAll(".000Z", ""));
                Date fechaInicial = format.parse(ts_fecha.replaceAll("T", " ").replaceAll(".000Z", ""));
                
                int diferencia=(int) ((fechaFinal.getTime()-fechaInicial.getTime())/1000);
                int dias=0;
                int horas=0;
                int minutos=0;
                
                if(diferencia>86400) {
                    dias=(int)Math.floor(diferencia/86400);
                    diferencia=diferencia-(dias*86400);
                }
                if(diferencia>3600) {
                    horas=(int)Math.floor(diferencia/3600);
                    diferencia=diferencia-(horas*3600);
                }
                if(diferencia>60) {
                    minutos=(int)Math.floor(diferencia/60);
                    diferencia=diferencia-(minutos*60);
                }
                
                tiempo_ganada =  dias+" dias, "+horas+" horas, "+minutos+" minutos";    
                
            }
            
        }catch(Exception ex){
            System.err.println("Error en tiempoGanadaToActivacion : " + ts_fecha + " ## " + fechaActivacion);    
        }
        
        return tiempo_ganada;         
    }
    
    public static String tiempoEntreFechas(String fecha_inicio,String fechaFin){
    
        String tiempo_ganada = "";
        
        try{
            
            if(fecha_inicio!= null && fechaFin!= null){
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                
                Date fechaFinal = format.parse(fechaFin.replaceAll("T", " "));
                Date fechaInicial = format.parse(fecha_inicio.replaceAll("T", " "));
                
                int diferencia=(int) ((fechaFinal.getTime()-fechaInicial.getTime())/1000);
                int dias=0;
                int horas=0;
                int minutos=0;
                
                if(diferencia>86400) {
                    dias=(int)Math.floor(diferencia/86400);
                    diferencia=diferencia-(dias*86400);
                }
                if(diferencia>3600) {
                    horas=(int)Math.floor(diferencia/3600);
                    diferencia=diferencia-(horas*3600);
                }
                if(diferencia>60) {
                    minutos=(int)Math.floor(diferencia/60);
                    diferencia=diferencia-(minutos*60);
                }
                
                tiempo_ganada =  dias+" dias, "+horas+" horas, "+minutos+" minutos";    
                
            }
            
        }catch(Exception ex){
            System.err.println("Error en tiempoGanadaToActivacion : " + fecha_inicio + " ## " + fechaFin);    
        }
        
        return tiempo_ganada;         
    }
    
    public static String horasGanadaToActivacion(String ts_fecha,String fechaActivacion){
    
        String tiempo_ganada = "";
        
        try{
            
            if(ts_fecha!= null && fechaActivacion!= null){
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                
                Date fechaFinal = format.parse(fechaActivacion.replaceAll("T", " ").replaceAll(".000Z", ""));
                Date fechaInicial = format.parse(ts_fecha.replaceAll("T", " ").replaceAll(".000Z", ""));
                
                int diferencia=(int) ((fechaFinal.getTime()-fechaInicial.getTime())/1000);
                int horas=0;
                
                horas=(int)Math.floor(diferencia/3600);
                    
                
                tiempo_ganada = String.valueOf(horas);    
                
            }
            
        }catch(Exception ex){
            System.err.println("Error en tiempoGanadaToActivacion : " + ts_fecha + " ## " + fechaActivacion);    
        }
        
        return tiempo_ganada;         
    }
    
    public static String tiempoTranscurrido(String fecha_creacion){
        
        try{
            
            if(fecha_creacion!=null){
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
                
                Date fechaFinal=new Date();
                Date fechaInicial=format.parse(fecha_creacion);
                
                int diferencia=(int) ((fechaFinal.getTime()-fechaInicial.getTime())/1000);
                int dias=0;
                int horas=0;
                int minutos=0;
                
                if(diferencia>86400) {
                    dias=(int)Math.floor(diferencia/86400);
                    diferencia=diferencia-(dias*86400);
                }
                if(diferencia>3600) {
                    horas=(int)Math.floor(diferencia/3600);
                    diferencia=diferencia-(horas*3600);
                }
                if(diferencia>60) {
                    minutos=(int)Math.floor(diferencia/60);
                    diferencia=diferencia-(minutos*60);
                }
                
                return dias+" dias, "+horas+" horas, "+minutos+" minutos";
            }else{
                return "NA";    
            }
           
        }catch(Exception ex){
            System.out.println("Error : transc " + ex.toString());
            return "No se pudo estimar el tiempo de demora";
        }
    }
    
    public static String semaforoDesvicacion(String fechaCerradaGanada,String fechaComprometida,String tipoCsp){
        
        String semaforo = "";
        
        try{
            Date fechaFinal= null;
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
            
            if(fechaComprometida!=null){
                if(!fechaComprometida.equals("NA")){
                    fechaFinal = format.parse(fechaCerradaGanada);              
                }else{
                    fechaFinal=new Date(); 
                }
            }else{
                fechaFinal=new Date(); 
            }
            
            Date fechaInicial=format.parse(fechaCerradaGanada);
            
            int diferencia=(int) ((fechaFinal.getTime()-fechaInicial.getTime())/1000);
            int dias=0;
            
            
            if(diferencia>86400) {
                dias=(int)Math.floor(diferencia/86400);
            }
            
            if(tipoCsp.equals(Constantes.C_LABEL_PAQUETE)){
                if(dias >=0 && dias<=5){
                    semaforo = Variables.getCOLOR_OK();
                }else if(dias >=6 && dias<=10){
                    semaforo = Variables.getCOLOR_M();
                }else if(dias >=11 && dias<=30){
                    semaforo = Variables.getCOLOR_B();
                }else{
                    semaforo = Variables.getCOLOR_SB();
                }
            }else{
                if(dias >=0 && dias<=20){
                    semaforo = Variables.getCOLOR_OK();
                }else if(dias >=21 && dias<=30){
                    semaforo = Variables.getCOLOR_M();
                }else if(dias >=31 && dias<=40){
                    semaforo = Variables.getCOLOR_B();
                }else{
                    semaforo = Variables.getCOLOR_SB();
                }    
            }
            
            return semaforo;
           
        }catch(Exception ex){
            System.out.println("Error tiempo de demora: " + ex.toString());
            return "No se pudo estimar el tiempo de demora";
        }
    }
    
    public static String getfechaObjectivo(String fechaGanada, String tipoPlan){
        String fechaObjectivo = "";
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);  
        try{
            
            if(!fechaGanada.equals("NA")){
                Date fechaInicial=format.parse(fechaGanada);
                calendar.setTime(fechaInicial);
                
                if(tipoPlan.equals(Constantes.C_LABEL_PAQUETE)){
                    calendar.add(Calendar.DAY_OF_YEAR, Integer.valueOf(Variables.getDIAS_PLAN()));    
                }else{
                    calendar.add(Calendar.DAY_OF_YEAR, Integer.valueOf(Variables.getDIAS_SOLUCION()));     
                }
                
                fechaObjectivo = dateFormat.format(calendar.getTime());
            }else{
                fechaObjectivo = "NA";   
            }
            
            
        }catch(Exception ex){
            console(Constantes.C_TYPE_GEN_ERR, "Error : " + ex.toString(), "getfechaObjectivo");
        }
        
        return dateFormat.format(calendar.getTime());
    }
    
    public static String formateaMontos(String monto){
        
        String numformat = "";
        DecimalFormat df = new DecimalFormat("#.00");
        try{
            
            numformat = df.format(Float.valueOf(monto));
        
        }catch(Exception ex){
            console(Constantes.C_TYPE_GEN_ERR, "Error : " + ex.toString(), "formateaMontos");
        }
        
        return numformat;        
    }
    
    public static String getDateSFSimple(String fechaAgenda_sf){
        
        try{
            
            if(fechaAgenda_sf!=null){
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);  
                Date date = sdf.parse(fechaAgenda_sf.replaceAll("T", " ").replaceAll(".000Z", ""));
                Calendar cal = Calendar.getInstance();
                cal.setTime(date);            
                return dateFormat.format(cal.getTime());     
            }else{
                return "NA";    
            }
            
        }catch(Exception ex){
            System.out.println("error fecha sf : " + ex.toString());
            return null;
        }
        
    }
    
    public static String getCleanDateSF(String fechaSf){
        
        try{
            
            if(fechaSf!=null){
                return fechaSf.replaceAll("T", " ").replaceAll(".000Z", "");     
            }else{
                return null;    
            }
            
        }catch(Exception ex){
            System.out.println("error fecha sf : " + ex.toString());
            return null;
        }
        
    }
    
    public static String getDateSF(String fechaAgenda_sf){
        
        try{
            
            if(fechaAgenda_sf!=null){
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.US);
                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);  
                Date date = sdf.parse(fechaAgenda_sf.replaceAll("T", " ").replaceAll(".000Z", ""));
                Calendar cal = Calendar.getInstance();
                cal.setTime(date);            
                return dateFormat.format(cal.getTime());     
            }else{
                return "NA";    
            }
            
        }catch(Exception ex){
            System.out.println("error fecha sf : " + ex.toString());
            return null;
        }
        
    }
    
    public static Map<String,String> fechasFiltroSf(String tipoFiltro,String fechaEntrada){
        Map<String,String> mapFechas = new HashMap<String,String>();
        
        try{
            
            if(Constantes.LABEL_SEMANA.equals(tipoFiltro)){
                mapFechas = rangoSemana(fechaEntrada);
            }else if(Constantes.LABEL_MES.equals(tipoFiltro)){
                mapFechas = rangoMes(fechaEntrada);    
            }else if(Constantes.LABEL_DIA.equals(tipoFiltro)){
                mapFechas = rangoUnDia(fechaEntrada);
            }
        
        }catch(Exception ex){
            System.err.println("Error al obtener filtros : " + ex.toString());  
        }
        
        return mapFechas;
    }
    
    
    public static Map<String,String> rangoUnDia(String fechaEntrada){
        
        Map<String,String> mapFechaSeleccionada = new HashMap<String,String>();
        
        try{
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date date = sdf.parse(fechaEntrada);
            Calendar c = Calendar.getInstance();
            c.setTime(date);
            mapFechaSeleccionada.put("fechaIn", sdf.format(c.getTime()) + "T00:00:00.000+0000");
            mapFechaSeleccionada.put("fechaFin", sdf.format(c.getTime()) + "T23:59:59.000+0000");
            
               
        }catch(Exception ex){
            System.err.println("Error en rango semana : " + ex.toString());
        }
        
        return mapFechaSeleccionada;
    }
    
    
    public static Map<String,String> rangoSemana(String fechaEntrada){
        
        Map<String,String> mapFechasSenama = new HashMap<String,String>();
        
        try{
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date date = sdf.parse(fechaEntrada);
            Calendar c = Calendar.getInstance();
            c.setTime(date);
            c.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
            mapFechasSenama.put("fechaIn", sdf.format(c.getTime()) + "T00:00:00.000+0000");
            c.add(Calendar.DAY_OF_WEEK, 6);
            mapFechasSenama.put("fechaFin", sdf.format(c.getTime()) + "T23:59:59.000+0000");
            c.add(Calendar.DAY_OF_WEEK, 1); 
               
        }catch(Exception ex){
            System.err.println("Error en rango semana : " + ex.toString());
        }
        
        return mapFechasSenama;
    }
    
    public static  Map<String,String> rangoMes(String fechaEntrada){
        
        Map<String,String> mapPrimerDia = new HashMap<String,String>();
        
        try{
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date date = sdf.parse(fechaEntrada);
            Calendar fecha = Calendar.getInstance();
            fecha.setTime(date);
            
           
            fecha.set(Calendar.DAY_OF_MONTH,1);
            mapPrimerDia.put("fechaIn", sdf.format(fecha.getTime()) + "T00:00:00.000+0000");
            
            fecha.set(Calendar.MONTH, fecha.get(Calendar.MONTH)+1);
            fecha.set(Calendar.DAY_OF_MONTH,-1);
            mapPrimerDia.put("fechaFin", sdf.format(fecha.getTime()) + "T23:59:59.000+0000");

        }catch(Exception ex){
            System.err.println("Error en primer dia mes : " + ex.toString());
        }
            
        return mapPrimerDia;
    }
    
    public static String estatusCsp(String statusOS){
        
        String estatusCsp = "";
        
        try{
            
            if(statusOS.equals(Constantes.C_OS_PENDIENTE) || statusOS.equals(Constantes.C_OS_PROCESO) || statusOS.equals(Constantes.C_OS_INICIO) || statusOS.equals(Constantes.C_OS_AGENDADO) || statusOS.equals(Constantes.C_OS_CONFIRMADO)){
                estatusCsp = Constantes.C_POR_INSTALAR;
            }else if(statusOS.equals(Constantes.C_OS_SUSPENDIDA) || statusOS.equals(Constantes.C_OS_CALENDARIZADO)){
                estatusCsp = Constantes.C_CALENDARIZADO;    
            }else if(statusOS.equals(Constantes.C_OS_GESTORIA) || statusOS.equals(Constantes.C_OS_DETENIDA)){
                estatusCsp = Constantes.C_DETENIDO;    
            }else if(statusOS.equals(Constantes.C_OS_RESCATE)){
                estatusCsp = Constantes.C_DEVUELTO_VENTAS;    
            }else if(statusOS.equals(Constantes.C_OS_CANCELADO)){
                estatusCsp = Constantes.C_CANCELADO;
            }else if(statusOS.equals(Constantes.C_OS_COMPLETADO)){
                estatusCsp = Constantes.C_INSTALADO;    
            }else{
                System.out.println("Estatus no identificado : " + statusOS);    
            }
            
        }catch(Exception ex){
            console(Constantes.C_TYPE_GEN_ERR, "Error : " + ex.toString(), "estatusCsp");
        }
        
        return estatusCsp;
    }
    
    public static Boolean getEventoDetonadoHoy(String fecha){
        
        Boolean bandera = false;
        
        try{
            
            if(fecha!=null){
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                SimpleDateFormat inputSfFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                Date date = new Date();
                            
                if(sdf.format(date).equals(sdf.format(inputSfFormat.parse(fecha)))){
                    bandera = true;    
                }
            }
            
        }catch(Exception ex){
            LocalUtils.console(Constantes.C_TYPE_GEN_ERR, "Error al evaluar dia : " + ex.toString(),  "getEventoDetonadoHoy");
        }
        
        return bandera;  
    }
}
