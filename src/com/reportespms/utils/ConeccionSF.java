package com.reportespms.utils;

import com.connectionsfffm.objetos.OutputsFFM;
import com.connectionsfffm.objetos.OutputsSF;
import com.connectionsfffm.utils.CrudSF;

import com.reportespms.baseobjects.Constantes;
import com.reportespms.baseobjects.Variables;

import com.sforce.soap.partner.LoginResult;
import com.sforce.soap.partner.PartnerConnection;
import com.sforce.soap.partner.QueryResult;
import com.sforce.soap.partner.sobject.SObject;
import com.sforce.ws.ConnectorConfig;

public class ConeccionSF {
    private CrudSF crudSf;
        
        public ConeccionSF() {
            super();
            this.crudSf = new CrudSF(Variables.getSF_US(),Variables.getSF_PA(),Variables.getSF_END_POINT(), Constantes.SF_QR_VALIDATE);
        }
        
        public OutputsSF consultaSalesforce(String queryString){
            
            OutputsSF out_sf = new OutputsSF();
            
            try{
                out_sf = crudSf.query_sf(queryString, this.getSessionIDSF());
            
            }catch(Exception ex){
                System.err.println("Error al ejecutar consulta sf : " + ex.toString());   
            }
            
            return out_sf;
                
        }
        
        public OutputsSF busquedaSalesforce(String queryString){
            
            OutputsSF out_sf = new OutputsSF();
            
            try{
                out_sf = crudSf.search_sf(queryString, this.getSessionIDSF());
            
            }catch(Exception ex){
                System.err.println("Error al ejecutar search sf : " + ex.toString());   
            }
            
            return out_sf;
                
        }
        
        public OutputsSF editaSalesforce(SObject[] objetos_actualizar){
            
            OutputsSF out_sf = new OutputsSF();
            
            try{
                out_sf = crudSf.update_sf(objetos_actualizar, this.getSessionIDSF());
            
            }catch(Exception ex){
                System.err.println("Error al ejecutar edicion sf : " + ex.toString());   
            }
            
            return out_sf;
                
        }
        
        public OutputsSF creaSalesforce(SObject[] objetos_crear){
            
            OutputsSF out_sf = new OutputsSF();
            
            try{
                out_sf = crudSf.create_sf(objetos_crear, this.getSessionIDSF());
            
            }catch(Exception ex){
                System.err.println("Error al ejecutar creacion sf : " + ex.toString());   
            }
            
            return out_sf;
                
        }
        
    public String getSessionIDSF() {
                    
        String sessionId = "";
        
        try {
            
            OutputsFFM out_conector_ffm = Constantes.CONECTION_FFM.consultaBD(Constantes.CS_ID_SESSION_SF_DB);
            String session_id_bd = "-1";
            
            if(out_conector_ffm.getResult()){
                if(out_conector_ffm.getArr_records().length > 0){
                    session_id_bd = out_conector_ffm.getArr_records()[0][0];
                }
            }
            
            if(session_id_bd.equals("") || session_id_bd.equals("-1") || session_id_bd == null ){
                PartnerConnection conexion_sf  = this.config_conector_sf();
                LoginResult login = conexion_sf.login(Variables.getSF_US(), Variables.getSF_PA());
                sessionId = login.getSessionId();
                
                Constantes.CONECTION_FFM.actualizaDB(Constantes.QR_UPDATE_SI_SF.replaceAll("sid_sf", sessionId));
            }else{
                sessionId = session_id_bd;
            }
            
            //-- valida que el session id recuperado de bd esta activo
            if(!this.validaSessionID(sessionId)){
                PartnerConnection conexion_sf  = this.config_conector_sf();
                LoginResult login = conexion_sf.login(Variables.getSF_US(), Variables.getSF_PA());
                sessionId = login.getSessionId();
                
                Constantes.CONECTION_FFM.actualizaDB(Constantes.QR_UPDATE_SI_SF.replaceAll("sid_sf", sessionId));
            }
            
            
        }catch (Exception e) {
        System.out.println("Errro al generar sesion id " + e.toString());
        }
        
        return sessionId;
    }
    
    public Boolean validaSessionID(String sessionID){
        Boolean bandera = false;
        
        try {
            
            QueryResult response_qr_sf = new QueryResult();
            PartnerConnection conexion_sf = this.conexion_conector_sf(sessionID) ;
            response_qr_sf = conexion_sf.query(Constantes.SF_QR_VALIDATE);
                
            bandera =  true;    
            
        } catch (Exception e) {
            bandera = false;
        }
        
        System.out.println("Valor de la bandera : " + bandera);
         
        return bandera;
    }
    
    public PartnerConnection config_conector_sf(){
        
        PartnerConnection conexion_sf;
        
        try{
            
            ConnectorConfig config = new ConnectorConfig();
            config.setUsername(Variables.getSF_US());
            config.setPassword(Variables.getSF_PA());
            config.setAuthEndpoint(Variables.getSF_END_POINT());
            
            conexion_sf = new PartnerConnection(config);
            
        }catch(Exception ex){
            System.out.println("Error al generar Conexion SF : " + ex.toString());
            conexion_sf = null;
        }
        
        return conexion_sf;
        
    }
    
    public PartnerConnection conexion_conector_sf(String id_sesion_sf){
        PartnerConnection conexion_sf;
        
        try{
            
            ConnectorConfig config = new ConnectorConfig();
            config.setUsername(Variables.getSF_US());
            config.setPassword(Variables.getSF_PA());
            config.setServiceEndpoint(Variables.getSF_END_POINT());
            config.setSessionId(id_sesion_sf);
            
            conexion_sf = new PartnerConnection(config);
            
        }catch(Exception ex){
            System.out.println("Error al generar Conexion SF : " + ex.toString());
            conexion_sf = null;
        }
        
        return conexion_sf;
    }
}
