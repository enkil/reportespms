package com.reportespms.utils;

import com.reportespms.baseobjects.Constantes;

import java.sql.Connection;
import java.sql.DriverManager;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

public class ConectorDB {
    
    public ConectorDB() {
        super();
    }
    
    public Connection creaConexion(String tipo){
        
        try{
            
           if(tipo.equals("JNDI")){
                 return this.crearConexionJNDI();
            }else if(tipo.equals("JDBC")){
                return this.creaConexionJDBC();
            }else{
                return null;    
            }
        
        }catch(Exception ex){
            System.err.println(ex.toString());
            return null;
        }
        
    }
    
    public Connection crearConexionJNDI(){
        
        try{  
            
            Context ctx = new InitialContext();
            DataSource ds = (DataSource) ctx.lookup(Constantes.DB_JNDI);                
                  
            return ds.getConnection();
                    
            }catch(Exception ex){
                System.err.println("Error al crear la conexion = " + ex);
                return null;
            }
    }
    
    public Connection creaConexionJDBC(){
        
        try{
            
            Connection conn = null;
            conn = DriverManager.getConnection("jdbc:oracle:thin:@"+Constantes.DB_HOST+":"+Constantes.DB_PORT+":"+Constantes.DB_DBNAME,Constantes.DB_US,Constantes.DB_PWR);
            
            if(conn != null){
              //  System.out.println("Conexion creada");    
            }else{
                System.out.println("No se pudo obtener la conexion");        
            }
            
            return conn;
        
        }catch(Exception ex){
            System.out.println("Error al crear conexion : " +ex.toString());
            return null;         
        }
                        
    }
    
    public Boolean cerrarConexion(Connection con){
        try{
            
            if(con != null){
                con.close();
                //System.out.println("La conexion se cerro correctamente");
            }else{
                System.out.println("La conexion es nula");
            }
            
            return true;
        }catch(Exception ex){
            System.err.println("Error al cerrar conexion = " + ex);
            return false;
        }        
    }
}
