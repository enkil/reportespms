package com.reportespms.objects;

public class OT {
    
    private String idOt;
    private String idTipoIntervencion;
    private String id_SubTipoIntervencion;
    private String desEstatusOt;
    private String desEstadoOt;
    private String desMotivoOt;
    private String fechaPrimerAgendaOt;
    private String fechaAgendaOt;
    private String fechaTerminoOt;
    
    public OT() {
        super();
    }

    public void setIdOt(String idOt) {
        this.idOt = idOt;
    }

    public String getIdOt() {
        return idOt;
    }

    public void setIdTipoIntervencion(String idTipoIntervencion) {
        this.idTipoIntervencion = idTipoIntervencion;
    }

    public String getIdTipoIntervencion() {
        return idTipoIntervencion;
    }

    public void setId_SubTipoIntervencion(String id_SubTipoIntervencion) {
        this.id_SubTipoIntervencion = id_SubTipoIntervencion;
    }

    public String getId_SubTipoIntervencion() {
        return id_SubTipoIntervencion;
    }

    public void setDesEstatusOt(String desEstatusOt) {
        this.desEstatusOt = desEstatusOt;
    }

    public String getDesEstatusOt() {
        return desEstatusOt;
    }

    public void setDesEstadoOt(String desEstadoOt) {
        this.desEstadoOt = desEstadoOt;
    }

    public String getDesEstadoOt() {
        return desEstadoOt;
    }

    public void setDesMotivoOt(String desMotivoOt) {
        this.desMotivoOt = desMotivoOt;
    }

    public String getDesMotivoOt() {
        return desMotivoOt;
    }

    public void setFechaPrimerAgendaOt(String fechaPrimerAgendaOt) {
        this.fechaPrimerAgendaOt = fechaPrimerAgendaOt;
    }

    public String getFechaPrimerAgendaOt() {
        return fechaPrimerAgendaOt;
    }

    public void setFechaAgendaOt(String fechaAgendaOt) {
        this.fechaAgendaOt = fechaAgendaOt;
    }

    public String getFechaAgendaOt() {
        return fechaAgendaOt;
    }

    public void setFechaTerminoOt(String fechaTerminoOt) {
        this.fechaTerminoOt = fechaTerminoOt;
    }

    public String getFechaTerminoOt() {
        return fechaTerminoOt;
    }
}
