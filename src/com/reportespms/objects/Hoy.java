package com.reportespms.objects;

public class Hoy {
    
    private String idBrinco;
    private String tipoNotificacion;
    private String nivelUno;
    private String nivelDos;
    private String nivelTres;
    private String nivelCuatro;
    private String colorNotificacionTxt;
    private String colorNotificacionBg;
    private String descNotificacion;
    
    public Hoy() {
        super();
    }

    public void setIdBrinco(String idBrinco) {
        this.idBrinco = idBrinco;
    }

    public String getIdBrinco() {
        return idBrinco;
    }

    public void setTipoNotificacion(String tipoNotificacion) {
        this.tipoNotificacion = tipoNotificacion;
    }

    public String getTipoNotificacion() {
        return tipoNotificacion;
    }

    public void setNivelUno(String nivelUno) {
        this.nivelUno = nivelUno;
    }

    public String getNivelUno() {
        return nivelUno;
    }

    public void setNivelDos(String nivelDos) {
        this.nivelDos = nivelDos;
    }

    public String getNivelDos() {
        return nivelDos;
    }

    public void setNivelTres(String nivelTres) {
        this.nivelTres = nivelTres;
    }

    public String getNivelTres() {
        return nivelTres;
    }

    public void setNivelCuatro(String nivelCuatro) {
        this.nivelCuatro = nivelCuatro;
    }

    public String getNivelCuatro() {
        return nivelCuatro;
    }

    public void setColorNotificacionTxt(String colorNotificacionTxt) {
        this.colorNotificacionTxt = colorNotificacionTxt;
    }

    public String getColorNotificacionTxt() {
        return colorNotificacionTxt;
    }

    public void setColorNotificacionBg(String colorNotificacionBg) {
        this.colorNotificacionBg = colorNotificacionBg;
    }

    public String getColorNotificacionBg() {
        return colorNotificacionBg;
    }

    public void setDescNotificacion(String descNotificacion) {
        this.descNotificacion = descNotificacion;
    }

    public String getDescNotificacion() {
        return descNotificacion;
    }
}
