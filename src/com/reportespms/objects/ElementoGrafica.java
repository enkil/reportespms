package com.reportespms.objects;

public class ElementoGrafica {
    
    private String nombreX;
    private String contadorOK;
    private String colorOK;
    private String contadorMD;
    private String colorMD;
    private String contadorMA;
    private String colorMA;
    private String contadorDV;
    private String colorDV;
    
    public ElementoGrafica() {
        super();
    }

    public void setNombreX(String nombreX) {
        this.nombreX = nombreX;
    }

    public String getNombreX() {
        return nombreX;
    }

    public void setContadorOK(String contadorOK) {
        this.contadorOK = contadorOK;
    }

    public String getContadorOK() {
        return contadorOK;
    }

    public void setContadorMD(String contadorMD) {
        this.contadorMD = contadorMD;
    }

    public String getContadorMD() {
        return contadorMD;
    }

    public void setContadorMA(String contadorMA) {
        this.contadorMA = contadorMA;
    }

    public String getContadorMA() {
        return contadorMA;
    }

    public void setContadorDV(String contadorDV) {
        this.contadorDV = contadorDV;
    }

    public String getContadorDV() {
        return contadorDV;
    }

    public void setColorOK(String colorOK) {
        this.colorOK = colorOK;
    }

    public String getColorOK() {
        return colorOK;
    }

    public void setColorMD(String colorMD) {
        this.colorMD = colorMD;
    }

    public String getColorMD() {
        return colorMD;
    }

    public void setColorMA(String colorMA) {
        this.colorMA = colorMA;
    }

    public String getColorMA() {
        return colorMA;
    }

    public void setColorDV(String colorDV) {
        this.colorDV = colorDV;
    }

    public String getColorDV() {
        return colorDV;
    }
}
