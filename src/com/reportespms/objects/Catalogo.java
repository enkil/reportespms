package com.reportespms.objects;

public class Catalogo {
    private String id;
    private String descripcion;
    private String nivel;
    private String idPadre;
    
    public String getId() {
            return id;
    }
    public void setId(String id) {
            this.id = id;
    }
    public String getDescripcion() {
            return descripcion;
    }
    public void setDescripcion(String descripcion) {
            this.descripcion = descripcion;
    }
    public String getNivel() {
            return nivel;
    }
    public void setNivel(String nivel) {
            this.nivel = nivel;
    }
    public String getIdPadre() {
            return idPadre;
    }
    public void setIdPadre(String idPadre) {
            this.idPadre = idPadre;
    }
}
