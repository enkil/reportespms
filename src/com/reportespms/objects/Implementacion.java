package com.reportespms.objects;

public class Implementacion {
    
    private String idCsp;
    private String folioCsp;
    private String idOtFfm;
    private String comentariosOs;
    private String nombreCliente;
    private String idCotizacion;                                 
    private String folioCotizacion;
    private String top5mil;
    private String segmentoGerencia;
    private String nombreContactoPrincipal;
    private String telefonoContactoPrincipal;
    private String idOs;
    private String folioOs;
    private String estatusOs;
    private String labelInicio;
    private String fechaInicio;
    private String labelAgendado;
    private String fechaAgendado;
    private String labelConfirmado;
    private String fechaConfirmado;
    private String labelCompletado;
    private String fechaCompletado;
    private String labelRescate;
    private String fechaRescate;
    private String labelCancelado;
    private String fechaCancelado;
    private String labelEnProceso;
    private String fechaEnProceso;
    private String labelCalendarizado;
    private String fechaCalendarizado;
    private String labelDetenida;
    private String fechaDetenida;
    private String labelSsuspendido;
    private String fechaSsuspendido;
    private String labelGestoria;
    private String fechaGestoria;
    
    public Implementacion() {
        super();
    }

    public void setIdCsp(String idCsp) {
        this.idCsp = idCsp;
    }

    public String getIdCsp() {
        return idCsp;
    }

    public void setFolioCsp(String folioCsp) {
        this.folioCsp = folioCsp;
    }

    public String getFolioCsp() {
        return folioCsp;
    }

    public void setIdOtFfm(String idOtFfm) {
        this.idOtFfm = idOtFfm;
    }

    public String getIdOtFfm() {
        return idOtFfm;
    }

    public void setComentariosOs(String comentariosOs) {
        this.comentariosOs = comentariosOs;
    }

    public String getComentariosOs() {
        return comentariosOs;
    }

    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    public String getNombreCliente() {
        return nombreCliente;
    }

    public void setIdCotizacion(String idCotizacion) {
        this.idCotizacion = idCotizacion;
    }

    public String getIdCotizacion() {
        return idCotizacion;
    }

    public void setFolioCotizacion(String folioCotizacion) {
        this.folioCotizacion = folioCotizacion;
    }

    public String getFolioCotizacion() {
        return folioCotizacion;
    }

    public void setTop5mil(String top5mil) {
        this.top5mil = top5mil;
    }

    public String getTop5mil() {
        return top5mil;
    }

    public void setSegmentoGerencia(String segmentoGerencia) {
        this.segmentoGerencia = segmentoGerencia;
    }

    public String getSegmentoGerencia() {
        return segmentoGerencia;
    }

    public void setNombreContactoPrincipal(String nombreContactoPrincipal) {
        this.nombreContactoPrincipal = nombreContactoPrincipal;
    }

    public String getNombreContactoPrincipal() {
        return nombreContactoPrincipal;
    }

    public void setTelefonoContactoPrincipal(String telefonoContactoPrincipal) {
        this.telefonoContactoPrincipal = telefonoContactoPrincipal;
    }

    public String getTelefonoContactoPrincipal() {
        return telefonoContactoPrincipal;
    }

    public void setIdOs(String idOs) {
        this.idOs = idOs;
    }

    public String getIdOs() {
        return idOs;
    }

    public void setFolioOs(String folioOs) {
        this.folioOs = folioOs;
    }

    public String getFolioOs() {
        return folioOs;
    }

    public void setFechaInicio(String fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public String getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaAgendado(String fechaAgendado) {
        this.fechaAgendado = fechaAgendado;
    }

    public String getFechaAgendado() {
        return fechaAgendado;
    }

    public void setFechaConfirmado(String fechaConfirmado) {
        this.fechaConfirmado = fechaConfirmado;
    }

    public String getFechaConfirmado() {
        return fechaConfirmado;
    }

    public void setFechaCompletado(String fechaCompletado) {
        this.fechaCompletado = fechaCompletado;
    }

    public String getFechaCompletado() {
        return fechaCompletado;
    }

    public void setFechaRescate(String fechaRescate) {
        this.fechaRescate = fechaRescate;
    }

    public String getFechaRescate() {
        return fechaRescate;
    }

    public void setFechaCancelado(String fechaCancelado) {
        this.fechaCancelado = fechaCancelado;
    }

    public String getFechaCancelado() {
        return fechaCancelado;
    }

    public void setFechaEnProceso(String fechaEnProceso) {
        this.fechaEnProceso = fechaEnProceso;
    }

    public String getFechaEnProceso() {
        return fechaEnProceso;
    }

    public void setFechaCalendarizado(String fechaCalendarizado) {
        this.fechaCalendarizado = fechaCalendarizado;
    }

    public String getFechaCalendarizado() {
        return fechaCalendarizado;
    }

    public void setFechaDetenida(String fechaDetenida) {
        this.fechaDetenida = fechaDetenida;
    }

    public String getFechaDetenida() {
        return fechaDetenida;
    }

    public void setFechaSsuspendido(String fechaSsuspendido) {
        this.fechaSsuspendido = fechaSsuspendido;
    }

    public String getFechaSsuspendido() {
        return fechaSsuspendido;
    }

    public void setFechaGestoria(String fechaGestoria) {
        this.fechaGestoria = fechaGestoria;
    }

    public String getFechaGestoria() {
        return fechaGestoria;
    }

    public void setEstatusOs(String estatusOs) {
        this.estatusOs = estatusOs;
    }

    public String getEstatusOs() {
        return estatusOs;
    }

    public void setLabelInicio(String labelInicio) {
        this.labelInicio = labelInicio;
    }

    public String getLabelInicio() {
        return labelInicio;
    }

    public void setLabelAgendado(String labelAgendado) {
        this.labelAgendado = labelAgendado;
    }

    public String getLabelAgendado() {
        return labelAgendado;
    }

    public void setLabelConfirmado(String labelConfirmado) {
        this.labelConfirmado = labelConfirmado;
    }

    public String getLabelConfirmado() {
        return labelConfirmado;
    }

    public void setLabelCompletado(String labelCompletado) {
        this.labelCompletado = labelCompletado;
    }

    public String getLabelCompletado() {
        return labelCompletado;
    }

    public void setLabelRescate(String labelRescate) {
        this.labelRescate = labelRescate;
    }

    public String getLabelRescate() {
        return labelRescate;
    }

    public void setLabelCancelado(String labelCancelado) {
        this.labelCancelado = labelCancelado;
    }

    public String getLabelCancelado() {
        return labelCancelado;
    }

    public void setLabelEnProceso(String labelEnProceso) {
        this.labelEnProceso = labelEnProceso;
    }

    public String getLabelEnProceso() {
        return labelEnProceso;
    }

    public void setLabelCalendarizado(String labelCalendarizado) {
        this.labelCalendarizado = labelCalendarizado;
    }

    public String getLabelCalendarizado() {
        return labelCalendarizado;
    }

    public void setLabelDetenida(String labelDetenida) {
        this.labelDetenida = labelDetenida;
    }

    public String getLabelDetenida() {
        return labelDetenida;
    }

    public void setLabelSsuspendido(String labelSsuspendido) {
        this.labelSsuspendido = labelSsuspendido;
    }

    public String getLabelSsuspendido() {
        return labelSsuspendido;
    }

    public void setLabelGestoria(String labelGestoria) {
        this.labelGestoria = labelGestoria;
    }

    public String getLabelGestoria() {
        return labelGestoria;
    }
}
