package com.reportespms.objects;

public class GraficaPM {
    
    private String totalPuntas;
    private String totalRenta;
    private String totalPuntasPorInstalar;
    private String totalRentaPorInstalar;
    private String totalPuntasCalendarizado;
    private String totalRentaCalendarizado;
    private String totalPuntasDetenido;
    private String totalRentaDetenido;
    private String totalPuntasDevueltoVentas;
    private String totalRentaDevueltoVentas;
    private String totalPuntasCancelado;
    private String totalRentaCancelado;
    private String totalPuntasInstalado;
    private String totalRentaInstalado;
    //--Grafica mes anterior
    private ElementoGrafica[] detalleGrafica;
    private String totalRentaInstaladoMA;
    private String totalPuntasInstaladoMA;
    private String totalRentaBackMA;
    private String totalPuntasBackMA;
    private String PorInstalarMA;
    private String CalendarizadoMA;
    private String DetenidoMA;
    private String DevueltoVentasMA;
    private String CanceladoMA;
            
    public GraficaPM() {
        super();
    }

    public void setTotalPuntas(String totalPuntas) {
        this.totalPuntas = totalPuntas;
    }

    public String getTotalPuntas() {
        return totalPuntas;
    }

    public void setTotalRenta(String totalRenta) {
        this.totalRenta = totalRenta;
    }

    public String getTotalRenta() {
        return totalRenta;
    }

    public void setTotalPuntasPorInstalar(String totalPuntasPorInstalar) {
        this.totalPuntasPorInstalar = totalPuntasPorInstalar;
    }

    public String getTotalPuntasPorInstalar() {
        return totalPuntasPorInstalar;
    }

    public void setTotalRentaPorInstalar(String totalRentaPorInstalar) {
        this.totalRentaPorInstalar = totalRentaPorInstalar;
    }

    public String getTotalRentaPorInstalar() {
        return totalRentaPorInstalar;
    }

    public void setTotalPuntasCalendarizado(String totalPuntasCalendarizado) {
        this.totalPuntasCalendarizado = totalPuntasCalendarizado;
    }

    public String getTotalPuntasCalendarizado() {
        return totalPuntasCalendarizado;
    }

    public void setTotalRentaCalendarizado(String totalRentaCalendarizado) {
        this.totalRentaCalendarizado = totalRentaCalendarizado;
    }

    public String getTotalRentaCalendarizado() {
        return totalRentaCalendarizado;
    }

    public void setTotalPuntasDetenido(String totalPuntasDetenido) {
        this.totalPuntasDetenido = totalPuntasDetenido;
    }

    public String getTotalPuntasDetenido() {
        return totalPuntasDetenido;
    }

    public void setTotalRentaDetenido(String totalRentaDetenido) {
        this.totalRentaDetenido = totalRentaDetenido;
    }

    public String getTotalRentaDetenido() {
        return totalRentaDetenido;
    }

    public void setTotalPuntasDevueltoVentas(String totalPuntasDevueltoVentas) {
        this.totalPuntasDevueltoVentas = totalPuntasDevueltoVentas;
    }

    public String getTotalPuntasDevueltoVentas() {
        return totalPuntasDevueltoVentas;
    }

    public void setTotalRentaDevueltoVentas(String totalRentaDevueltoVentas) {
        this.totalRentaDevueltoVentas = totalRentaDevueltoVentas;
    }

    public String getTotalRentaDevueltoVentas() {
        return totalRentaDevueltoVentas;
    }

    public void setTotalPuntasCancelado(String totalPuntasCancelado) {
        this.totalPuntasCancelado = totalPuntasCancelado;
    }

    public String getTotalPuntasCancelado() {
        return totalPuntasCancelado;
    }

    public void setTotalRentaCancelado(String totalRentaCancelado) {
        this.totalRentaCancelado = totalRentaCancelado;
    }

    public String getTotalRentaCancelado() {
        return totalRentaCancelado;
    }

    public void setTotalPuntasInstalado(String totalPuntasInstalado) {
        this.totalPuntasInstalado = totalPuntasInstalado;
    }

    public String getTotalPuntasInstalado() {
        return totalPuntasInstalado;
    }

    public void setTotalRentaInstalado(String totalRentaInstalado) {
        this.totalRentaInstalado = totalRentaInstalado;
    }

    public String getTotalRentaInstalado() {
        return totalRentaInstalado;
    }

    public void setDetalleGrafica(ElementoGrafica[] detalleGrafica) {
        this.detalleGrafica = detalleGrafica;
    }

    public ElementoGrafica[] getDetalleGrafica() {
        return detalleGrafica;
    }

    public void setTotalRentaInstaladoMA(String totalRentaInstaladoMA) {
        this.totalRentaInstaladoMA = totalRentaInstaladoMA;
    }

    public String getTotalRentaInstaladoMA() {
        return totalRentaInstaladoMA;
    }

    public void setTotalRentaBackMA(String totalRentaBackMA) {
        this.totalRentaBackMA = totalRentaBackMA;
    }

    public String getTotalRentaBackMA() {
        return totalRentaBackMA;
    }

    public void setPorInstalarMA(String PorInstalarMA) {
        this.PorInstalarMA = PorInstalarMA;
    }

    public String getPorInstalarMA() {
        return PorInstalarMA;
    }

    public void setCalendarizadoMA(String CalendarizadoMA) {
        this.CalendarizadoMA = CalendarizadoMA;
    }

    public String getCalendarizadoMA() {
        return CalendarizadoMA;
    }

    public void setDetenidoMA(String DetenidoMA) {
        this.DetenidoMA = DetenidoMA;
    }

    public String getDetenidoMA() {
        return DetenidoMA;
    }

    public void setDevueltoVentasMA(String DevueltoVentasMA) {
        this.DevueltoVentasMA = DevueltoVentasMA;
    }

    public String getDevueltoVentasMA() {
        return DevueltoVentasMA;
    }

    public void setCanceladoMA(String CanceladoMA) {
        this.CanceladoMA = CanceladoMA;
    }

    public String getCanceladoMA() {
        return CanceladoMA;
    }

    public void setTotalPuntasInstaladoMA(String totalPuntasInstaladoMA) {
        this.totalPuntasInstaladoMA = totalPuntasInstaladoMA;
    }

    public String getTotalPuntasInstaladoMA() {
        return totalPuntasInstaladoMA;
    }

    public void setTotalPuntasBackMA(String totalPuntasBackMA) {
        this.totalPuntasBackMA = totalPuntasBackMA;
    }

    public String getTotalPuntasBackMA() {
        return totalPuntasBackMA;
    }

}
