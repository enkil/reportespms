package com.reportespms.objects;

public class Planeacion {
    
    private String idPuenteSf;
    private String estatusPlaneacion;
    private String desviacionDias;
    private String semaforoDesviacion;
    private String fechaObjetivo;
    private String semanaObjetivo;
    private String fechaComprometida;
    private String dependencias;
    private String numEmpleadoResponsable;
    private String nombreResponsable;    
    private String comentarios;   
    
    public Planeacion() {
        super();
    }


    public void setEstatusPlaneacion(String estatusPlaneacion) {
        this.estatusPlaneacion = estatusPlaneacion;
    }

    public String getEstatusPlaneacion() {
        return estatusPlaneacion;
    }

    public void setDesviacionDias(String desviacionDias) {
        this.desviacionDias = desviacionDias;
    }

    public String getDesviacionDias() {
        return desviacionDias;
    }

    public void setSemaforoDesviacion(String semaforoDesviacion) {
        this.semaforoDesviacion = semaforoDesviacion;
    }

    public String getSemaforoDesviacion() {
        return semaforoDesviacion;
    }

    public void setFechaObjetivo(String fechaObjetivo) {
        this.fechaObjetivo = fechaObjetivo;
    }

    public String getFechaObjetivo() {
        return fechaObjetivo;
    }

    public void setSemanaObjetivo(String semanaObjetivo) {
        this.semanaObjetivo = semanaObjetivo;
    }

    public String getSemanaObjetivo() {
        return semanaObjetivo;
    }

    public void setFechaComprometida(String fechaComprometida) {
        this.fechaComprometida = fechaComprometida;
    }

    public String getFechaComprometida() {
        return fechaComprometida;
    }

    public void setDependencias(String dependencias) {
        this.dependencias = dependencias;
    }

    public String getDependencias() {
        return dependencias;
    }

    public void setNombreResponsable(String nombreResponsable) {
        this.nombreResponsable = nombreResponsable;
    }

    public String getNombreResponsable() {
        return nombreResponsable;
    }

    public void setComentarios(String comentarios) {
        this.comentarios = comentarios;
    }

    public String getComentarios() {
        return comentarios;
    }

    public void setNumEmpleadoResponsable(String numEmpleadoResponsable) {
        this.numEmpleadoResponsable = numEmpleadoResponsable;
    }

    public String getNumEmpleadoResponsable() {
        return numEmpleadoResponsable;
    }

    public void setIdPuenteSf(String idPuenteSf) {
        this.idPuenteSf = idPuenteSf;
    }

    public String getIdPuenteSf() {
        return idPuenteSf;
    }
}
