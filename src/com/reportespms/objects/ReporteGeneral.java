package com.reportespms.objects;

public class ReporteGeneral {
    
    private String cliente;
    private String nombreOportunidad;
    private String numeroOportunidad ;
    private String folioCotizacion;
    private String idCotSitio;
    private String folioCotSitio;
    private String idCotSitioPlan;
    private String folioCsp;
    private String estatusCsp;
    private String cuentaFactura;    
    private String nombreSitio;
    private String tipoCotizacion;
    private String tipoVenta;
    private String tipoplan;
    private String plazaComercia;
    private String nombreVendedor;
    private String plazaVendedor;   
    private String nombrePm;
    private String gerencia;
    private String region;
    private String ciudad;
    private String distrito;
    private String cluster;
    private String direccionSitio;
    private String plan;
    private String segmento;
    private String topCincomil;
    private String precioRenta;
    private String precioRentaConInpuestos;
    private String folioOs;
    private String estatatusOs;
    private String idOt;
    private String tipoCuadrilla;
    private String tipoIntervencion;
    private String subTipoIntervencion;
    private String estatusOt;
    private String estadoOt;
    private String motivoOt;
    private String fechaVenta;
    private String fechaCerradaGanada;
    private String fechaAgendamiento;
    private String fechaInstalacion;
    private String fechaActivacion;
    private String semanaVenta;
    private String semanaCerradaGanada;
    private String semanaAgenda;
    private String semanaInstalacion;
    private String tiempoVentaToInstalacion;
    private String tiempoGanadaToInstalacion;
    private String estatusPlaneacion;
    private String desviacionDias;
    private String semaforoDesviacion;
    private String fechaObjetivo;
    private String semanaObjetivo;
    private String fechaComprometida;
    private String dependencias;
    private String numeroEmpleadoResponsable;
    private String nombreResponsable;
    private String comentarios;
    
    
    public ReporteGeneral() {
        super();
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public String getCliente() {
        return cliente;
    }

    public void setNombreOportunidad(String nombreOportunidad) {
        this.nombreOportunidad = nombreOportunidad;
    }

    public String getNombreOportunidad() {
        return nombreOportunidad;
    }

    public void setNumeroOportunidad(String numeroOportunidad) {
        this.numeroOportunidad = numeroOportunidad;
    }

    public String getNumeroOportunidad() {
        return numeroOportunidad;
    }

    public void setFolioCotizacion(String folioCotizacion) {
        this.folioCotizacion = folioCotizacion;
    }

    public String getFolioCotizacion() {
        return folioCotizacion;
    }

    public void setFolioCsp(String folioCsp) {
        this.folioCsp = folioCsp;
    }

    public String getFolioCsp() {
        return folioCsp;
    }

    public void setCuentaFactura(String cuentaFactura) {
        this.cuentaFactura = cuentaFactura;
    }

    public String getCuentaFactura() {
        return cuentaFactura;
    }

    public void setNombreSitio(String nombreSitio) {
        this.nombreSitio = nombreSitio;
    }

    public String getNombreSitio() {
        return nombreSitio;
    }

    public void setTipoCotizacion(String tipoCotizacion) {
        this.tipoCotizacion = tipoCotizacion;
    }

    public String getTipoCotizacion() {
        return tipoCotizacion;
    }

    public void setTipoVenta(String tipoVenta) {
        this.tipoVenta = tipoVenta;
    }

    public String getTipoVenta() {
        return tipoVenta;
    }

    public void setPlazaComercia(String plazaComercia) {
        this.plazaComercia = plazaComercia;
    }

    public String getPlazaComercia() {
        return plazaComercia;
    }

    public void setNombreVendedor(String nombreVendedor) {
        this.nombreVendedor = nombreVendedor;
    }

    public String getNombreVendedor() {
        return nombreVendedor;
    }

    public void setPlazaVendedor(String plazaVendedor) {
        this.plazaVendedor = plazaVendedor;
    }

    public String getPlazaVendedor() {
        return plazaVendedor;
    }

    public void setNombrePm(String nombrePm) {
        this.nombrePm = nombrePm;
    }

    public String getNombrePm() {
        return nombrePm;
    }

    public void setGerencia(String gerencia) {
        this.gerencia = gerencia;
    }

    public String getGerencia() {
        return gerencia;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getRegion() {
        return region;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setDistrito(String distrito) {
        this.distrito = distrito;
    }

    public String getDistrito() {
        return distrito;
    }

    public void setCluster(String cluster) {
        this.cluster = cluster;
    }

    public String getCluster() {
        return cluster;
    }

    public void setDireccionSitio(String direccionSitio) {
        this.direccionSitio = direccionSitio;
    }

    public String getDireccionSitio() {
        return direccionSitio;
    }

    public void setPlan(String plan) {
        this.plan = plan;
    }

    public String getPlan() {
        return plan;
    }

    public void setSegmento(String segmento) {
        this.segmento = segmento;
    }

    public String getSegmento() {
        return segmento;
    }

    public void setTopCincomil(String topCincomil) {
        this.topCincomil = topCincomil;
    }

    public String getTopCincomil() {
        return topCincomil;
    }

    public void setPrecioRenta(String precioRenta) {
        this.precioRenta = precioRenta;
    }

    public String getPrecioRenta() {
        return precioRenta;
    }

    public void setPrecioRentaConInpuestos(String precioRentaConInpuestos) {
        this.precioRentaConInpuestos = precioRentaConInpuestos;
    }

    public String getPrecioRentaConInpuestos() {
        return precioRentaConInpuestos;
    }

    public void setFolioOs(String folioOs) {
        this.folioOs = folioOs;
    }

    public String getFolioOs() {
        return folioOs;
    }

    public void setEstatatusOs(String estatatusOs) {
        this.estatatusOs = estatatusOs;
    }

    public String getEstatatusOs() {
        return estatatusOs;
    }

    public void setIdOt(String idOt) {
        this.idOt = idOt;
    }

    public String getIdOt() {
        return idOt;
    }

    public void setTipoCuadrilla(String tipoCuadrilla) {
        this.tipoCuadrilla = tipoCuadrilla;
    }

    public String getTipoCuadrilla() {
        return tipoCuadrilla;
    }

    public void setTipoIntervencion(String tipoIntervencion) {
        this.tipoIntervencion = tipoIntervencion;
    }

    public String getTipoIntervencion() {
        return tipoIntervencion;
    }

    public void setSubTipoIntervencion(String subTipoIntervencion) {
        this.subTipoIntervencion = subTipoIntervencion;
    }

    public String getSubTipoIntervencion() {
        return subTipoIntervencion;
    }

    public void setEstatusOt(String estatusOt) {
        this.estatusOt = estatusOt;
    }

    public String getEstatusOt() {
        return estatusOt;
    }

    public void setEstadoOt(String estadoOt) {
        this.estadoOt = estadoOt;
    }

    public String getEstadoOt() {
        return estadoOt;
    }

    public void setMotivoOt(String motivoOt) {
        this.motivoOt = motivoOt;
    }

    public String getMotivoOt() {
        return motivoOt;
    }

    public void setFechaVenta(String fechaVenta) {
        this.fechaVenta = fechaVenta;
    }

    public String getFechaVenta() {
        return fechaVenta;
    }

    public void setFechaCerradaGanada(String fechaCerradaGanada) {
        this.fechaCerradaGanada = fechaCerradaGanada;
    }

    public String getFechaCerradaGanada() {
        return fechaCerradaGanada;
    }

    public void setFechaAgendamiento(String fechaAgendamiento) {
        this.fechaAgendamiento = fechaAgendamiento;
    }

    public String getFechaAgendamiento() {
        return fechaAgendamiento;
    }

    public void setFechaInstalacion(String fechaInstalacion) {
        this.fechaInstalacion = fechaInstalacion;
    }

    public String getFechaInstalacion() {
        return fechaInstalacion;
    }

    public void setFechaActivacion(String fechaActivacion) {
        this.fechaActivacion = fechaActivacion;
    }

    public String getFechaActivacion() {
        return fechaActivacion;
    }

    public void setSemanaVenta(String semanaVenta) {
        this.semanaVenta = semanaVenta;
    }

    public String getSemanaVenta() {
        return semanaVenta;
    }

    public void setSemanaCerradaGanada(String semanaCerradaGanada) {
        this.semanaCerradaGanada = semanaCerradaGanada;
    }

    public String getSemanaCerradaGanada() {
        return semanaCerradaGanada;
    }

    public void setSemanaAgenda(String semanaAgenda) {
        this.semanaAgenda = semanaAgenda;
    }

    public String getSemanaAgenda() {
        return semanaAgenda;
    }

    public void setSemanaInstalacion(String semanaInstalacion) {
        this.semanaInstalacion = semanaInstalacion;
    }

    public String getSemanaInstalacion() {
        return semanaInstalacion;
    }

    public void setTiempoVentaToInstalacion(String tiempoVentaToInstalacion) {
        this.tiempoVentaToInstalacion = tiempoVentaToInstalacion;
    }

    public String getTiempoVentaToInstalacion() {
        return tiempoVentaToInstalacion;
    }

    public void setTiempoGanadaToInstalacion(String tiempoGanadaToInstalacion) {
        this.tiempoGanadaToInstalacion = tiempoGanadaToInstalacion;
    }

    public String getTiempoGanadaToInstalacion() {
        return tiempoGanadaToInstalacion;
    }

    public void setEstatusPlaneacion(String estatusPlaneacion) {
        this.estatusPlaneacion = estatusPlaneacion;
    }

    public String getEstatusPlaneacion() {
        return estatusPlaneacion;
    }

    public void setDesviacionDias(String desviacionDias) {
        this.desviacionDias = desviacionDias;
    }

    public String getDesviacionDias() {
        return desviacionDias;
    }

    public void setSemaforoDesviacion(String semaforoDesviacion) {
        this.semaforoDesviacion = semaforoDesviacion;
    }

    public String getSemaforoDesviacion() {
        return semaforoDesviacion;
    }

    public void setFechaObjetivo(String fechaObjetivo) {
        this.fechaObjetivo = fechaObjetivo;
    }

    public String getFechaObjetivo() {
        return fechaObjetivo;
    }

    public void setSemanaObjetivo(String semanaObjetivo) {
        this.semanaObjetivo = semanaObjetivo;
    }

    public String getSemanaObjetivo() {
        return semanaObjetivo;
    }

    public void setFechaComprometida(String fechaComprometida) {
        this.fechaComprometida = fechaComprometida;
    }

    public String getFechaComprometida() {
        return fechaComprometida;
    }

    public void setDependencias(String dependencias) {
        this.dependencias = dependencias;
    }

    public String getDependencias() {
        return dependencias;
    }

    public void setNombreResponsable(String nombreResponsable) {
        this.nombreResponsable = nombreResponsable;
    }

    public String getNombreResponsable() {
        return nombreResponsable;
    }

    public void setComentarios(String comentarios) {
        this.comentarios = comentarios;
    }

    public String getComentarios() {
        return comentarios;
    }

    public void setFolioCotSitio(String folioCotSitio) {
        this.folioCotSitio = folioCotSitio;
    }

    public String getFolioCotSitio() {
        return folioCotSitio;
    }

    public void setIdCotSitio(String idCotSitio) {
        this.idCotSitio = idCotSitio;
    }

    public String getIdCotSitio() {
        return idCotSitio;
    }

    public void setIdCotSitioPlan(String idCotSitioPlan) {
        this.idCotSitioPlan = idCotSitioPlan;
    }

    public String getIdCotSitioPlan() {
        return idCotSitioPlan;
    }

    public void setTipoplan(String tipoplan) {
        this.tipoplan = tipoplan;
    }

    public String getTipoplan() {
        return tipoplan;
    }

    public void setNumeroEmpleadoResponsable(String numeroEmpleadoResponsable) {
        this.numeroEmpleadoResponsable = numeroEmpleadoResponsable;
    }

    public String getNumeroEmpleadoResponsable() {
        return numeroEmpleadoResponsable;
    }

    public void setEstatusCsp(String estatusCsp) {
        this.estatusCsp = estatusCsp;
    }

    public String getEstatusCsp() {
        return estatusCsp;
    }
}
