package com.reportespms.objects;

public class Proyecto {
    
    private String idCotizacion;
    private String folioCotizacion;
    private String puntasTotales;
    private String nombreCliente;
    private String top5mil;
    private String segmentoGerencia;
    private String nombreContactoPrincipal;
    private String telefonoContactoPrincipal;
    private String fechaCerradaGanada;
    private String fechaAsignacion;
    private String segmento;
    private String plaza;
    private String nombreVendedor;
    private String telefonoVendedor;
    private String emailVendedor;
    
    public Proyecto() {
        super();
    }

    public void setIdCotizacion(String idCotizacion) {
        this.idCotizacion = idCotizacion;
    }

    public String getIdCotizacion() {
        return idCotizacion;
    }

    public void setFolioCotizacion(String folioCotizacion) {
        this.folioCotizacion = folioCotizacion;
    }

    public String getFolioCotizacion() {
        return folioCotizacion;
    }

    public void setPuntasTotales(String puntasTotales) {
        this.puntasTotales = puntasTotales;
    }

    public String getPuntasTotales() {
        return puntasTotales;
    }

    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    public String getNombreCliente() {
        return nombreCliente;
    }

    public void setTop5mil(String top5mil) {
        this.top5mil = top5mil;
    }

    public String getTop5mil() {
        return top5mil;
    }

    public void setSegmentoGerencia(String segmentoGerencia) {
        this.segmentoGerencia = segmentoGerencia;
    }

    public String getSegmentoGerencia() {
        return segmentoGerencia;
    }

    public void setNombreContactoPrincipal(String nombreContactoPrincipal) {
        this.nombreContactoPrincipal = nombreContactoPrincipal;
    }

    public String getNombreContactoPrincipal() {
        return nombreContactoPrincipal;
    }

    public void setTelefonoContactoPrincipal(String telefonoContactoPrincipal) {
        this.telefonoContactoPrincipal = telefonoContactoPrincipal;
    }

    public String getTelefonoContactoPrincipal() {
        return telefonoContactoPrincipal;
    }

    public void setFechaCerradaGanada(String fechaCerradaGanada) {
        this.fechaCerradaGanada = fechaCerradaGanada;
    }

    public String getFechaCerradaGanada() {
        return fechaCerradaGanada;
    }

    public void setSegmento(String segmento) {
        this.segmento = segmento;
    }

    public String getSegmento() {
        return segmento;
    }

    public void setPlaza(String plaza) {
        this.plaza = plaza;
    }

    public String getPlaza() {
        return plaza;
    }

    public void setNombreVendedor(String nombreVendedor) {
        this.nombreVendedor = nombreVendedor;
    }

    public String getNombreVendedor() {
        return nombreVendedor;
    }

    public void setTelefonoVendedor(String telefonoVendedor) {
        this.telefonoVendedor = telefonoVendedor;
    }

    public String getTelefonoVendedor() {
        return telefonoVendedor;
    }

    public void setEmailVendedor(String emailVendedor) {
        this.emailVendedor = emailVendedor;
    }

    public String getEmailVendedor() {
        return emailVendedor;
    }

    public void setFechaAsignacion(String fechaAsignacion) {
        this.fechaAsignacion = fechaAsignacion;
    }

    public String getFechaAsignacion() {
        return fechaAsignacion;
    }
}
