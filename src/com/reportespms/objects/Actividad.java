package com.reportespms.objects;

public class Actividad {
    
    private String idBridgeSf;
    private String idActividad;
    private String descripcionActividad;
    private String tieneOt;
    private String fechaInicioPlabeada;
    private String fechaFinPlaneada;
    private String fechaInicioReal;
    private String fechaFinReal;
    private String avance;
    private String idDependencia;
    private String nombreResponsable;
    private String telefonoResponsable;
    private String estatusPlaneacion;
    private String fechaActual;
    private String idBrinco;
    private String tipoNotificacion;
    private String nivelUno;
    private String nivelDos;
    private String nivelTres;
    private String nivelCuatro;
    private String colorNotificacionTxt;
    private String colorNotificacionBg;
    private String descNotificacion;
    
    public Actividad() {
        super();
    }

    public void setIdBridgeSf(String idBridgeSf) {
        this.idBridgeSf = idBridgeSf;
    }

    public String getIdBridgeSf() {
        return idBridgeSf;
    }

    public void setIdActividad(String idActividad) {
        this.idActividad = idActividad;
    }

    public String getIdActividad() {
        return idActividad;
    }

    public void setDescripcionActividad(String descripcionActividad) {
        this.descripcionActividad = descripcionActividad;
    }

    public String getDescripcionActividad() {
        return descripcionActividad;
    }

    public void setTieneOt(String tieneOt) {
        this.tieneOt = tieneOt;
    }

    public String getTieneOt() {
        return tieneOt;
    }

    public void setFechaInicioPlabeada(String fechaInicioPlabeada) {
        this.fechaInicioPlabeada = fechaInicioPlabeada;
    }

    public String getFechaInicioPlabeada() {
        return fechaInicioPlabeada;
    }

    public void setFechaFinPlaneada(String fechaFinPlaneada) {
        this.fechaFinPlaneada = fechaFinPlaneada;
    }

    public String getFechaFinPlaneada() {
        return fechaFinPlaneada;
    }

    public void setFechaInicioReal(String fechaInicioReal) {
        this.fechaInicioReal = fechaInicioReal;
    }

    public String getFechaInicioReal() {
        return fechaInicioReal;
    }

    public void setFechaFinReal(String fechaFinReal) {
        this.fechaFinReal = fechaFinReal;
    }

    public String getFechaFinReal() {
        return fechaFinReal;
    }

    public void setAvance(String avance) {
        this.avance = avance;
    }

    public String getAvance() {
        return avance;
    }

    public void setIdDependencia(String idDependencia) {
        this.idDependencia = idDependencia;
    }

    public String getIdDependencia() {
        return idDependencia;
    }

    public void setNombreResponsable(String nombreResponsable) {
        this.nombreResponsable = nombreResponsable;
    }

    public String getNombreResponsable() {
        return nombreResponsable;
    }

    public void setTelefonoResponsable(String telefonoResponsable) {
        this.telefonoResponsable = telefonoResponsable;
    }

    public String getTelefonoResponsable() {
        return telefonoResponsable;
    }

    public void setEstatusPlaneacion(String estatusPlaneacion) {
        this.estatusPlaneacion = estatusPlaneacion;
    }

    public String getEstatusPlaneacion() {
        return estatusPlaneacion;
    }

    public void setFechaActual(String fechaActual) {
        this.fechaActual = fechaActual;
    }

    public String getFechaActual() {
        return fechaActual;
    }

    public void setIdBrinco(String idBrinco) {
        this.idBrinco = idBrinco;
    }

    public String getIdBrinco() {
        return idBrinco;
    }

    public void setTipoNotificacion(String tipoNotificacion) {
        this.tipoNotificacion = tipoNotificacion;
    }

    public String getTipoNotificacion() {
        return tipoNotificacion;
    }

    public void setNivelUno(String nivelUno) {
        this.nivelUno = nivelUno;
    }

    public String getNivelUno() {
        return nivelUno;
    }

    public void setNivelDos(String nivelDos) {
        this.nivelDos = nivelDos;
    }

    public String getNivelDos() {
        return nivelDos;
    }

    public void setNivelTres(String nivelTres) {
        this.nivelTres = nivelTres;
    }

    public String getNivelTres() {
        return nivelTres;
    }

    public void setNivelCuatro(String nivelCuatro) {
        this.nivelCuatro = nivelCuatro;
    }

    public String getNivelCuatro() {
        return nivelCuatro;
    }

    public void setColorNotificacionTxt(String colorNotificacionTxt) {
        this.colorNotificacionTxt = colorNotificacionTxt;
    }

    public String getColorNotificacionTxt() {
        return colorNotificacionTxt;
    }

    public void setColorNotificacionBg(String colorNotificacionBg) {
        this.colorNotificacionBg = colorNotificacionBg;
    }

    public String getColorNotificacionBg() {
        return colorNotificacionBg;
    }

    public void setDescNotificacion(String descNotificacion) {
        this.descNotificacion = descNotificacion;
    }

    public String getDescNotificacion() {
        return descNotificacion;
    }
}
